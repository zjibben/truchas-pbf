!!
!! VOF_INIT
!!
!! This module provides routines for initializing the
!! VOF scalar based on shapes provided by the user
!!
!! Zechariah J. Jibben <zjibben@lanl.gov>
!! January 2018
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module vof_init

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use material_geometry_type
  use logging_services
  implicit none
  private

  type :: dnc_cell
    private
    real(r8) :: x(3,8)
    integer :: matl_at_node(8), matl_at_subnode(8,8), recursion_limit
    type(material_geometry), pointer :: matl_geom ! reference only
  contains
    procedure :: init => dnc_cell_init
    procedure :: vof
    procedure, private :: vof_from_nodes
    procedure, private :: subcell
    procedure, private :: set_division_variables
    procedure, private :: contains_interface
  end type dnc_cell

  public :: vof_initialize

contains

  ! this subroutine initializes the vof values in all cells
  ! from a user input function. It calls a divide and conquer algorithm
  ! to calculate the volume fractions given an interface
  subroutine vof_initialize(mesh, matl_init_geometry, recursion_limit, nmat, vof)

    use amrex_mesh_type
    use amrex_base_module, only: amrex_mfiter, amrex_multifab, amrex_box
    use parameter_list_type
    use timer_tree_type
    use mpi

    type(amrex_mesh), intent(in) :: mesh
    type(material_geometry), target, intent(in) :: matl_init_geometry
    integer, intent(in) :: recursion_limit, nmat
    type(amrex_multifab), intent(inout) :: vof

    real(r8), allocatable :: vof_tmp(:)
    real(r8), pointer :: vof_data(:,:,:,:)
    integer :: ix,iy,iz, this_rank, ierr
    type(amrex_mfiter) :: mfi
    type(dnc_cell) :: cell
    type(amrex_box) :: bx

    call MPI_Comm_rank(mesh%comm, this_rank, ierr)
    if (this_rank==0) print '(a)', "initializing vof ... "
    call start_timer("vof init")

    ! nmat = size(matl_ids)
    ! call matl_init_geometry%init(plist, matl_ids)

    call mesh%mfiter_build(mfi)
    call vof%setval(0.0_r8)  ! valid fp values on domain ghosts
    do while (mfi%next())
      bx = mfi%tilebox()
      vof_data => vof%dataptr(mfi)

      do iz = bx%lo(3),bx%hi(3)
        do iy = bx%lo(2),bx%hi(2)
          do ix = bx%lo(1),bx%hi(1)
            call cell%init([ix,iy,iz], recursion_limit, mesh, matl_init_geometry)
            vof_tmp = cell%vof(nmat, 0)
            vof_data(ix,iy,iz,1) = vof_tmp(1)
          end do
        end do
      end do
    end do

    call vof%fill_boundary(mesh%geom)

    call stop_timer("vof init")
    if (this_rank==0) print '(a)', "done"

  end subroutine vof_initialize

  subroutine dnc_cell_init(this, ind, recursion_limit, mesh, matl_geom)

    use amrex_mesh_type

    class(dnc_cell), intent(out) :: this
    integer, intent(in) :: ind(:), recursion_limit
    type(amrex_mesh), intent(in) :: mesh
    type(material_geometry), target, intent(in) :: matl_geom

    integer :: v

    this%recursion_limit = recursion_limit
    this%matl_geom => matl_geom

    ! set node positions
    this%x(:,1) = mesh%geom%get_physical_location(ind)
    this%x(:,2) = this%x(:,1) + [1.0_r8, 0.0_r8, 0.0_r8] * mesh%geom%dx
    this%x(:,3) = this%x(:,1) + [1.0_r8, 1.0_r8, 0.0_r8] * mesh%geom%dx
    this%x(:,4) = this%x(:,1) + [0.0_r8, 1.0_r8, 0.0_r8] * mesh%geom%dx
    this%x(:,5) = this%x(:,1) + [0.0_r8, 0.0_r8, 1.0_r8] * mesh%geom%dx
    this%x(:,6) = this%x(:,1) + [1.0_r8, 0.0_r8, 1.0_r8] * mesh%geom%dx
    this%x(:,7) = this%x(:,1) + [1.0_r8, 1.0_r8, 1.0_r8] * mesh%geom%dx
    this%x(:,8) = this%x(:,1) + [0.0_r8, 1.0_r8, 1.0_r8] * mesh%geom%dx

    ! get materials at node
    do v = 1,8
      this%matl_at_node(v) = this%matl_geom%index_at(this%x(:,v))
    end do

  end subroutine dnc_cell_init

  ! calculates the volume fractions of materials in a cell
  !
  ! check if all vertices lie within a single material.
  ! if so, set the Vof for that material to 1.0.
  ! if not, divide the cell into tets and repeat recursively to a given threshold
  recursive function vof(this, nmat, depth) result(hex_vof)

    class(dnc_cell), intent(inout) :: this
    integer, intent(in) :: nmat,depth
    real(r8) :: hex_vof(nmat)

    integer :: i
    type(dnc_cell) :: subcell

    ! if the cell contains an interface (and therefore has at least two materials
    ! and we haven't yet hit our recursion limit, divide the hex and repeat
    if (depth < this%recursion_limit .and. this%contains_interface()) then
      ! tally the vof from sub-cells
      call this%set_division_variables()
      hex_vof = 0
      do i = 1,8
        subcell = this%subcell(i)
        hex_vof = hex_vof + subcell%vof(nmat, depth+1) / 8
      end do
    else
      ! if we are past the recursion limit
      ! or the cell does not contain an interface, calculate
      ! the vof in this hex based on the materials at its nodes
      hex_vof = this%vof_from_nodes(nmat)
    end if

  end function vof

  pure logical function contains_interface(this)
    class(dnc_cell), intent(in) :: this
    contains_interface = any(this%matl_at_node /= this%matl_at_node(1))
  end function contains_interface

  ! calculate the volume fraction from nodes. used at the bottom depth
  !
  ! If we have >2 materials in this simulation, we need to calculate the vof
  ! from nodes only, using the average of the node materials. If we
  ! have 2 materials, we can reconstruct
  ! a plane from the signed distance function. Reconstructing the plane
  ! gives us much greater accuracy, which allows us to go to a lower depth
  ! and save significantly on computational cost.
  !
  ! TODO: We could switch this dynamically, so instead of looking at how many
  !       materials are in the entire simulation, we look at how many are in
  !       this cell. Then we can change the depth required for 2-phase cells,
  !       vs many-material cells.
  function vof_from_nodes(this, nmat)

    ! use plane_type
    ! use array_utils, only: normalize
    ! use cell_geometry, only: cross_product

    class(dnc_cell), intent(inout) :: this
    integer, intent(in) :: nmat
    real(r8) :: vof_from_nodes(nmat)

    integer :: m
    ! real(r8) :: x(3,3), s1, s2
    ! type(plane) :: P
    ! type(polyhedron) :: poly

    ! if (nmat > 2 .or. .not.this%contains_interface()) then
      vof_from_nodes = 0
      do m = 1,nmat
        vof_from_nodes(m) = count(this%matl_at_node == m)
      end do
      vof_from_nodes = vof_from_nodes / 8
    ! else
    !   ! get 3 points intersecting the edges of this cell
    !   i = 0
    !   do e = 1,12
    !     if (this%matl_at_node(this%geom%parent%edge_vid(1,e)) /= &
    !         this%matl_at_node(this%geom%parent%edge_vid(2,e))) then
    !       s1 = this%matl_geom%signed_distance(this%x(:,this%geom%parent%edge_vid(1,e)))
    !       s2 = this%matl_geom%signed_distance(this%x(:,this%geom%parent%edge_vid(2,e)))

    !       i = i + 1
    !       x(:,i) = this%x(:,this%geom%parent%edge_vid(1,e)) + &
    !           s1/(s1-s2) * (this%x(:,this%geom%parent%edge_vid(2,e)) - &
    !           this%x(:,this%geom%parent%edge_vid(1,e)))
    !       if (i == 3) exit
    !     end if
    !   end do
    !   ASSERT(e <= 12)
    !   if (e <= 12) then
    !     ! construct a plane from the 3 points
    !     P%normal = normalize(cross_product(x(:,2) - x(:,1), x(:,3) - x(:,1)))
    !     P%rho = dot_product(x(:,1), P%normal)

    !     ! find a node that is guaranteed not to intersect the interface
    !     ! flip the plane normal if it is outside the halfspace but inside the material
    !     do i = 1,8
    !       s1 = P%signed_distance(this%x(:,i))

    !       if (abs(s1) > 0) then
    !         ! WARN: right now this is hardwired for material 2 being the "droplet"
    !         !       we will need to fix this
    !         if ((s1 > 0 .and. this%matl_at_node(i) == 2) .or. &
    !             (s1 < 0 .and. this%matl_at_node(i) /= 2)) then
    !           P%normal = -P%normal
    !           P%rho = -P%rho
    !         end if
    !         exit
    !       end if
    !     end do

    !     ! get the volume fraction behind the plane in this cell
    !     ! copy into new polyhedron structure because face normals
    !     ! aren't calculated at each level of the initialization
    !     ! maybe a better way of getting around this?
    !     ! WARN: the hardwiring for material 2 as the droplet is also here
    !     call poly%init(ierr, this%x)
    !     vof_from_nodes(2) = poly%volume_behind_plane(P, ierr) / this%geom%volume()
    !     vof_from_nodes(1) = 1 - vof_from_nodes(2)
    !   else
    !     ! interface doesn't actually intersect this subcell
    !     ! could happen if it just barely touches an edge
    !     vof_from_nodes = 0
    !     vof_from_nodes(this%matl_at_node(1)) = 1
    !   end if
    ! end if

  end function vof_from_nodes

  subroutine set_division_variables (this)

    class(dnc_cell), intent(inout) :: this

    integer :: i,j

    do i = 1,8
      this%matl_at_subnode(i,i) = this%matl_at_node(i)
      do j = 1,8
        if (j/=i) &
            this%matl_at_subnode(j,i) = this%matl_geom%index_at((this%x(:,i) + this%x(:,j))/2)
      end do
    end do

  end subroutine set_division_variables

  type(dnc_cell) function subcell(this, i)

    class(dnc_cell), intent(in) :: this
    integer, intent(in) :: i

    integer :: j

    subcell%matl_geom => this%matl_geom
    subcell%recursion_limit = this%recursion_limit
    do j = 1,8
      subcell%x(:,j) = (this%x(:,i) + this%x(:,j)) / 2
      subcell%matl_at_node(j) = this%matl_at_subnode(j,i)
    end do

  end function subcell

end module vof_init
