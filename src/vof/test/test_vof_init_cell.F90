!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program test_vof_init

#ifdef NAGFOR
  use,intrinsic :: f90_unix, only: exit
#endif
  use,intrinsic :: iso_fortran_env, only: output_unit
  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use mpi
  use amrex_base_module
  use parameter_list_type
  use truchas_amrex_init_proc
  use amrex_mesh_type
  use logging_services
  implicit none

  integer :: nproc, this_rank, ierr, status = 0
  type(parameter_list) :: params
#if defined(__GFORTRAN__)
  procedure(amrex_finalize), pointer :: pp
#endif
  real(r8), parameter :: PI = 3.1415926535897932_r8

  call truchas_amrex_init(params)

#if defined(__GFORTRAN__)
  pp => amrex_finalize
  call LS_initialize([output_unit], verbosity=LS_VERB_NOISY, finalize=pp)
#else
  call LS_initialize([output_unit], verbosity=LS_VERB_NOISY, finalize=amrex_finalize)
#endif

  call MPI_Comm_rank(MPI_COMM_WORLD, this_rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)

  call cylinder_test()

  call amrex_finalize()
  call exit(status)

contains

  subroutine cylinder_test()

    use region_class
    use material_geometry_type
    use region_factories, only: alloc_cylinder_region, alloc_fill_region
    use scalar_func_class
    use scalar_func_containers, only: scalar_func_box
    use scalar_func_factories, only: alloc_const_scalar_func, alloc_piecewise_scalar_func
    use vof_init

    type(parameter_list) :: params
    type(amrex_mesh) :: mesh
    type(material_geometry) :: matl_geom
    type(region_box) :: rgn(2)
    type(scalar_func_box) :: subfunc(2)
    class(scalar_func), allocatable :: matl_index
    real(r8) :: vof_ex, vof_calc
    real(r8), pointer :: vof_data(:,:,:,:)
    type(amrex_multifab) :: vof
    type(amrex_mfiter) :: mfi

    ! set up mesh
    call params%set('lo', [1,1,1])
    call params%set('hi', [1,1,1])
    call params%set('box-size', 16)
    call params%set('prob-lo', [-0.06_r8, 0.24_r8, -5e-3_r8])
    call params%set('prob-hi', [-0.05_r8, 0.25_r8, 5e-3_r8])
    call mesh%init(params)

    ! set up the cylinder geometry
    call alloc_cylinder_region (rgn(1)%r, [0.0_r8, 0.0_r8, 0.0_r8], [0.0_r8, 0.0_r8, 1.0_r8], &
        0.25_r8, 1.0_r8)
    call alloc_fill_region (rgn(2)%r)

    call alloc_const_scalar_func (subfunc(1)%f, 1.0_r8)
    call alloc_const_scalar_func (subfunc(2)%f, 2.0_r8)

    call alloc_piecewise_scalar_func (matl_index, subfunc, rgn)
    call matl_geom%init(matl_index)

    ! calculate vof
    call mesh%multifab_build(vof, nc=1, ng=1)
    call vof_initialize(mesh, matl_geom, 6, 2, vof)

    call mesh%mfiter_build(mfi)
    do while (mfi%next())
      vof_data => vof%dataptr(mfi)
      vof_calc = vof_data(1,1,1,1)
    end do

    ! print results
    if (this_rank == 0) then
      vof_ex = 1 - 0.614298769812239_r8
      print '(a,3es15.5)', "vof, vofex, err: ", vof_calc, vof_ex, abs(vof_calc - vof_ex)
      if (abs(vof_calc - vof_ex) > 5e-5_r8) status = 1
    end if
    call MPI_Bcast(status, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)

  end subroutine cylinder_test

end program test_vof_init
