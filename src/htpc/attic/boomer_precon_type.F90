!!
!! BOOMER_PRECON_TYPE
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! December 2017
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! IMPLEMENTATION NOTES
!!
!! (1) The SStruct system interface is badly broken (2.11.2). One should be
!! able to create a matrix or vector once, and repeatedly reuse it by calling
!! SStructMatrixInitialize prior to redefining the matrix values. This leads
!! to memory leaks however (size of matrix), because internally the initialize
!! procedure calls IJMatrixCreate without freeing the memory allocated in
!! previous initialization calls.  The situation is similar (worse actually)
!! for vectors.  Consequently vectors and matrices are "single use"; they
!! must be destroyed and recreated to change their values.
!!
!! (2) The HYPRE_BoomerAMGSetup function requires the RHS and solution vectors,
!! presumably as will be passed to HYPRE_BoomerAMGSolve, as input arguments.
!! The reference manual indicates these are unused, however inspection of the
!! code suggests otherwise, at least in certain build configurations. Actually
!! doing this is not acceptable in light of (1).  Some testing suggests that
!! we can get away with passing null pointers for these arguments, and that is
!! what we do -- it may break things in the future.
!!

#include "f90_assert.fpp"

module boomer_precon_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use fhypre
  use amrex_mesh_type
  implicit none
  private

  type, public :: boomer_precon
    private
    integer :: comm ! MPI communicator
    type(amrex_mesh), pointer :: mesh => null() ! reference only -- not owned
    type(hypre_obj) :: grid = hypre_null_obj
    type(hypre_obj) :: graph = hypre_null_obj
    type(hypre_obj) :: solver = hypre_null_obj
    type(hypre_obj) :: A = hypre_null_obj
    ! BoomerAMG parameters
    integer  :: maxitr
    real(r8) :: threshold
    integer, allocatable :: coarsen_type
    ! For state save/restore
    type(hypre_obj) :: solver1 = hypre_null_obj
    type(hypre_obj) :: A1 = hypre_null_obj
  contains
    procedure :: init
    procedure :: setup
    procedure :: apply
    procedure :: create_vector
    procedure :: create_matrix
    procedure, nopass :: destroy_vector
    procedure, nopass :: destroy_matrix
    procedure :: copy_to_vector
    procedure :: copy_from_vector
    procedure :: save_state
    procedure :: restore_state
    final :: boomer_precon_delete
  end type boomer_precon

  !! Our current AMReX mesh corresponds to a 3D Hypre semi-structured grid
  !! composed of a single part having ID 0.
  integer, parameter :: NDIM = 3, NPARTS = 1, PART0 = 0

  !! The variables are x, y, and z face-centered (XFACE==2, YFACE==3, ZFACE==4)
  !! The ordering of local variables on a cell for the purposes of Hypre SStruct
  !! FEM procedures are low x-face, high x-face, low y-face, high y-face, etc.
  !! NB: This should be the default ordering, and the associated call unnecessary
  integer, parameter :: VARTYPES(*) = [2,3,4]
  integer, parameter :: ORDERING(*) = [0,-1,0,0,  0,+1,0,0,  1,0,-1,0, &
                                       1,0,+1,0,  2,0,0,-1,  2,0,0,+1]

  !! Some Hypre SStruct procedures operate with respect to a specific variable.
  integer, parameter, public :: XFACEVAR = 0, YFACEVAR = 1, ZFACEVAR = 2

contains

  !! Final subroutine for BOOMER_PRECON objects
  subroutine boomer_precon_delete(this)
    type(boomer_precon), intent(inout) :: this
    integer :: ierr
    if (hypre_associated(this%graph)) call fHYPRE_SStructGraphDestroy(this%graph, ierr)
    if (hypre_associated(this%grid)) call fHYPRE_SStructGridDestroy(this%grid, ierr)
    if (.not.hypre_associated(this%A,this%A1)) then
      if (hypre_associated(this%A1)) call fHYPRE_BoomerAMGDestroy(this%A1, ierr)
    end if
    if (hypre_associated(this%A)) call fHYPRE_SStructMatrixDestroy(this%A, ierr)
    if (.not.hypre_associated(this%solver,this%solver1)) then
      if (hypre_associated(this%solver1)) call fHYPRE_BoomerAMGDestroy(this%solver1, ierr)
    end if
    if (hypre_associated(this%solver)) call fHYPRE_BoomerAMGDestroy(this%solver, ierr)
  end subroutine boomer_precon_delete

  subroutine init(this, mesh, params, stat, errmsg)

    use parameter_list_type

    class(boomer_precon), intent(out) :: this
    type(amrex_mesh), intent(in), target :: mesh
    type(parameter_list) :: params
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    integer :: j, ierr
    character(:), allocatable :: context

    this%mesh => mesh
    this%comm = mesh%comm

    call fHYPRE_ClearAllErrors

    !! Create the SStruct grid object %GRID
    call fHYPRE_SStructGridCreate(mesh%comm, NDIM, NPARTS, this%grid, ierr)
    do j = 1, mesh%nbox
      call fHYPRE_SStructGridSetExtents(this%grid, PART0, mesh%box(j)%lo, mesh%box(j)%hi, ierr)
    end do
    call fHYPRE_SStructGridSetVariables(this%grid, PART0, VARTYPES, ierr)
    call fHYPRE_SStructGridSetFEMOrdering(this%grid, PART0, ORDERING, ierr)
    call fHYPRE_SStructGridAssemble(this%grid, ierr)
    INSIST(ierr == 0)

    !! Create the SStruct graph object %GRAPH
    call fHYPRE_SStructGraphCreate(mesh%comm, this%grid, this%graph, ierr)
    call fHYPRE_SStructGraphSetFEM(this%graph, PART0, ierr)
    call fHYPRE_SStructGraphSetObjectType(this%graph, HYPRE_PARCSR, ierr)
    call fHYPRE_SStructGraphAssemble(this%graph, ierr)
    INSIST(ierr == 0)

    !! Process the parameter list.
    context = 'processing ' // params%name() // ': '
    call params%get('num-cycles', this%maxitr, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    if (this%maxitr <= 0) then
      stat = 1
      errmsg = context // ' "num-cycles" must be > 0'
      return
    end if

    call params%get('strong-threshold', this%threshold, default=0.5_r8, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    if (this%threshold <= 0.0_r8) then
      stat = 1
      errmsg = context // ' "strong-threshold" must be > 0.0'
      return
    end if

    if (params%is_parameter('coarsen-type')) then
      allocate(this%coarsen_type)
      call params%get('coarsen-type', this%coarsen_type, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if
    end if

  end subroutine init

  subroutine setup(this, A)

    class(boomer_precon), intent(inout) :: this
    type(hypre_obj), intent(inout) :: A

    integer :: ierr
    type(hypre_obj) :: A_obj

    if (.not.hypre_associated(this%A,this%A1)) call destroy_matrix(this%A)
    this%A = A  ! we take ownership; this persists through multiple APPLY calls
    A = hypre_null_obj

    call fHYPRE_ClearAllErrors

    !! Create the Hypre solver object.  Once the solver has been setup, it is
    !! not possible to change the matrix values without completely destroying
    !! the solver and recreating it from scratch.
    if (.not.hypre_associated(this%solver,this%solver1)) call destroy_solver(this%solver)
    call fHYPRE_BoomerAMGCreate(this%solver, ierr)
    INSIST(ierr == 0)

    !! Set solver parameters.
    call fHYPRE_BoomerAMGSetTol(this%solver, 0.0_r8, ierr)  ! using as preconditioner
    call fHYPRE_BoomerAMGSetMaxIter(this%solver, this%maxitr, ierr)
    call fHYPRE_BoomerAMGSetStrongThreshold(this%solver, this%threshold, ierr)
    INSIST(ierr == 0)

    !! Set optional solver parameters (track evolving Hypre defaults).
    if (allocated(this%coarsen_type)) &
        call fHYPRE_BoomerAMGSetCoarsenType(this%solver, this%coarsen_type, ierr)
    INSIST(ierr == 0)

    !! Setup the solver; see Note 2.
    call fHYPRE_SStructMatrixGetObject(this%A, A_obj, ierr)
    call fHYPRE_BoomerAMGSetup(this%solver, A_obj, hypre_null_obj, hypre_null_obj, ierr)
    INSIST(ierr == 0)

  end subroutine setup

  !! Apply the preconditioner defined by the SETUP call to the B vector and
  !! return the result in the X vector.  Caller is responsible for B.  This
  !! subroutine creates X, and the caller is responsible for destroying it
  !! when done with it (and before passing to APPLY again; see the NB for
  !! CREATE_VECTOR.

  subroutine apply(this, b, x)

    class(boomer_precon), intent(inout) :: this
    type(hypre_obj), intent(in) :: b
    type(hypre_obj), intent(out) :: x

    integer :: ierr
    type(hypre_obj) :: A_obj, b_obj, x_obj

    call fHYPRE_ClearAllErrors

    !! Initialize the result vector to zero.
    call this%create_vector(x)
    call fHYPRE_SStructVectorAssemble(x, ierr)
    INSIST(ierr == 0)

    !! Precondition the B vector; result goes to X.
    call fHYPRE_SStructMatrixGetObject(this%A, A_obj, ierr)
    call fHYPRE_SStructVectorGetObject(b, b_obj, ierr)
    call fHYPRE_SStructVectorGetObject(x, x_obj, ierr)
    call fHYPRE_BoomerAMGSolve(this%solver, A_obj, b_obj, x_obj, ierr)
    INSIST(ierr == 0)

    !! Ensure shared face values are consistent.
    call fHYPRE_SStructVectorGather(x, ierr)
    INSIST(ierr == 0)

  end subroutine apply

  !! Create a SStruct VECTOR with PARCSR as the internal storage scheme.
  !! The vector is returned intialized and ready for setting its values.
  !! NB: The input vector handle is overwritten with the handle of the new
  !! vector.  To avoid a memory leak, ensure that an existing vector is
  !! destroyed before calling this subroutine, or remains accessible via
  !! another HYPRE_OBJ variable.

  subroutine create_vector(this, vector)
    class(boomer_precon), intent(in) :: this
    type(hypre_obj), intent(out) :: vector
    integer :: ierr
    call fHYPRE_ClearAllErrors
    call fHYPRE_SStructVectorCreate(this%comm, this%grid, vector, ierr)
    call fHYPRE_SStructVectorSetObjectType(vector, HYPRE_PARCSR, ierr)
    call fHYPRE_SStructVectorInitialize(vector, ierr)
    INSIST(ierr == 0)
  end subroutine create_vector

  subroutine destroy_vector(vector)
    type(hypre_obj), intent(inout) :: vector
    integer :: ierr
    call fHYPRE_ClearAllErrors
    call fHYPRE_SStructVectorDestroy(vector, ierr)
    INSIST(ierr == 0)
    vector = hypre_null_obj
  end subroutine destroy_vector

  !! Create a SStruct matrix with PARCSR as the internal storage scheme.
  !! The matrix is returned initialized and ready for setting its values.
  !! NB: The input matrix handle is overwritten with the handle of the new
  !! matrix.  To avoid a memory leak, ensure that an existing matrix is
  !! destroyed before calling this subroutine, or remains accessible via
  !! another HYPRE_OBJ variable.

  subroutine create_matrix(this, matrix)
    class(boomer_precon), intent(in) :: this
    type(hypre_obj), intent(out) :: matrix
    integer :: ierr
    call fHYPRE_ClearAllErrors
    call fHYPRE_SStructMatrixCreate(this%comm, this%graph, matrix, ierr)
    call fHYPRE_SStructMatrixSetObjectType(matrix, HYPRE_PARCSR, ierr)
    call fHYPRE_SStructMatrixInitialize(matrix, ierr)
    INSIST(ierr == 0)
  end subroutine create_matrix

  subroutine destroy_matrix(matrix)
    type(hypre_obj), intent(inout) :: matrix
    integer :: ierr
    if (hypre_associated(matrix)) then
      call fHYPRE_ClearAllErrors
      call fHYPRE_SStructMatrixDestroy(matrix, ierr)
      INSIST(ierr == 0)
      matrix = hypre_null_obj
    end if
  end subroutine destroy_matrix

  subroutine destroy_solver(solver)
    type(hypre_obj), intent(inout) :: solver
    integer :: ierr
    if (hypre_associated(solver)) then
      call fHYPRE_ClearAllErrors
      call fHYPRE_BoomerAMGDestroy(solver, ierr)
      INSIST(ierr == 0)
      solver = hypre_null_obj
    end if
  end subroutine

  !! Copy data from x, y, and z-face multifabs to a Hypre SStruct vector created
  !! by CREATE_VECTOR. The multifabs must have a single component and no ghosts
  !! (important!) and have synced shared values.  The Hypre vector should be
  !! initialized but not yet assembled.  This will set all data in the Hypre
  !! vector. The caller is responsible for assembling the vector.

  subroutine copy_to_vector(this, xmf, ymf, zmf, vector)

    use,intrinsic :: iso_fortran_env, only: r8 => real64
    use amrex_base_module

    class(boomer_precon), intent(in) :: this
    type(amrex_multifab), intent(in) :: xmf, ymf, zmf
    type(hypre_obj), intent(in) :: vector

    integer :: ierr
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: box
    real(r8), pointer, contiguous :: dp(:,:,:,:)

    call fHYPRE_ClearAllErrors

    call this%mesh%mfiter_build(mfi)
    do while (mfi%next())
      box = mfi%tilebox()
      dp => xmf%dataptr(mfi)
      call fHYPRE_SStructVectorSetBoxValues(vector, PART0, box%lo-[1,0,0], box%hi, XFACEVAR, dp, ierr)
      INSIST(ierr == 0)
      dp => ymf%dataptr(mfi)
      call fHYPRE_SStructVectorSetBoxValues(vector, PART0, box%lo-[0,1,0], box%hi, YFACEVAR, dp, ierr)
      INSIST(ierr == 0)
      dp => zmf%dataptr(mfi)
      call fHYPRE_SStructVectorSetBoxValues(vector, PART0, box%lo-[0,0,1], box%hi, ZFACEVAR, dp, ierr)
      INSIST(ierr == 0)
    end do

  end subroutine copy_to_vector

  !! Copy data from a Hypre SStruct vector created by CREATE_VECTOR to x, y, and
  !! z-face multifabs. The Hypre vector must have consistent shared data; caller
  !! should call fHYPRE_SStructVectorGather first if needed.  The multifabs must
  !! have a single component and no ghosts (important!).  This will set all data
  !! in the multifabs with consistent shared data; no call to FillBoundary nec.

  subroutine copy_from_vector(this, vector, xmf, ymf, zmf)

    use,intrinsic :: iso_fortran_env, only: r8 => real64
    use amrex_base_module

    class(boomer_precon), intent(in) :: this
    type(hypre_obj), intent(in) :: vector
    type(amrex_multifab), intent(inout) :: xmf, ymf, zmf

    integer :: ierr
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: box
    real(r8), pointer, contiguous :: dp(:,:,:,:)

    call this%mesh%mfiter_build(mfi)
    do while (mfi%next())
      box = mfi%tilebox()
      dp => xmf%dataptr(mfi)
      call fHYPRE_SStructVectorGetBoxValues(vector, PART0, box%lo-[1,0,0], box%hi, XFACEVAR, dp, ierr)
      INSIST(ierr == 0)
      dp => ymf%dataptr(mfi)
      call fHYPRE_SStructVectorGetBoxValues(vector, PART0, box%lo-[0,1,0], box%hi, YFACEVAR, dp, ierr)
      INSIST(ierr == 0)
      dp => zmf%dataptr(mfi)
      call fHYPRE_SStructVectorGetBoxValues(vector, PART0, box%lo-[0,0,1], box%hi, ZFACEVAR, dp, ierr)
      INSIST(ierr == 0)
    end do

  end subroutine copy_from_vector

  !! These routines copy the current solver and matrix handles to/from a save
  !! buffer, and are used to reinstate the state of the the boomer_precon object.
  !! Because multiple handle variable may refer to the same object, particular
  !! care must be taken to prevent memory leaks and objects from being destroyed
  !! prematurely.  This includes SETUP which creates new object.

  subroutine save_state(this)
    class(boomer_precon), intent(inout) :: this
    if (.not.hypre_associated(this%solver, this%solver1)) then
      call destroy_solver(this%solver1)
      this%solver1 = this%solver
    end if
    if (.not.hypre_associated(this%A, this%A1)) then
      call destroy_matrix(this%A1)
      this%A1 = this%A
    end if
  end subroutine save_state

  subroutine restore_state(this)
    class(boomer_precon), intent(inout) :: this
    if (.not.hypre_associated(this%solver, this%solver1)) then
      call destroy_solver(this%solver)
      this%solver = this%solver1
    end if
    if (.not.hypre_associated(this%A, this%A1)) then
      call destroy_matrix(this%A)
      this%A = this%A1
    end if
  end subroutine restore_state

end module boomer_precon_type
