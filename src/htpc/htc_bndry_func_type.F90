!! HTC_BNDRY_FUNC_TYPE
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! February 2018
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! TODO: The bulk of the init procedure will be common to any extension of the
!! base class.  This code defines the space coordinates and permuation for the
!! particular side.  This should be moved up to the base class and not duplicated.

#include "f90_assert.fpp"

module htc_bndry_func_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use bndry_func2_class
  use scalar_func_class
  implicit none
  private

  type, extends(bndry_func2), public :: htc_bndry_func
    private
    real(r8) :: a(3), b(3), area
    integer  :: perm(3)
    class(scalar_func), allocatable :: f, g
  contains
    procedure :: init
    procedure :: compute
    procedure :: compute_value => compute
    procedure :: compute_deriv => compute
  end type htc_bndry_func

contains

  subroutine init(this, geom, box, side, htc, tamb)

    use amrex_base_module, only: amrex_box, amrex_geometry

    class(htc_bndry_func), intent(out) :: this
    type(amrex_geometry), intent(in) :: geom
    type(amrex_box), intent(in) :: box
    character(3), intent(in) :: side
    class(scalar_func), intent(in) :: htc, tamb

    integer :: dir, lo(3), hi(3), nc(3)
    real(r8) :: xlo(3), xhi(3)

    xlo = geom%get_physical_location(box%lo)
    xhi = geom%get_physical_location(box%hi+1)
    this%a = xlo - 0.5_r8*geom%dx

    dir = scan('xyz',side(1:1))
    select case (side(2:3))
    case ('lo')
      this%a(dir) = xlo(dir)
    case ('hi')
      this%a(dir) = xhi(dir)
    case default
      INSIST(.false.)
    end select

    select case (dir)
    case (1)
      this%perm = [2,3,1]
    case (2)
      this%perm = [1,3,2]
    case (3)
      this%perm = [1,2,3]
    case default
      INSIST(.false.)
    end select

    this%a = this%a(this%perm)
    this%b = geom%dx(this%perm)
    this%area = this%b(1)*this%b(2)

    lo = box%lo(this%perm)
    hi = box%hi(this%perm)
    nc = hi - lo + 1
    allocate(this%value(nc(1),nc(2)), this%deriv(nc(1),nc(2)))

    allocate(this%f, source=htc)
    allocate(this%g, source=tamb)

  end subroutine init

  subroutine compute(this, t, var)
    class(htc_bndry_func), intent(inout) :: this
    real(r8), intent(in) :: t, var(:,:)
    integer :: j1, j2
    real(r8) :: args(0:3)
    ASSERT(all(shape(var) == shape(this%value)))
    args(0) = t; args(this%perm(3)) = this%a(3)
    do j2 = 1, size(this%value,dim=2)
      args(this%perm(2)) = this%a(2) + this%b(2)*j2
      do j1 = 1, size(this%value,dim=1)
        args(this%perm(1)) = this%a(1) + this%b(1)*j1
        this%value(j1,j2) = this%area*this%f%eval(args)*(var(j1,j2) - this%g%eval(args))
        this%deriv(j1,j2) = this%area*this%f%eval(args)
      end do
    end do
  end subroutine compute

end module htc_bndry_func_type
