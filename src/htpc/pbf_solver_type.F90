!!
!! PBF_SOLVER_TYPE
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! March 2019
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module pbf_solver_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use truchas_env_type
  use ml_mesh_type
  use amrex_mesh_type
  use pbf_model_type
  use ht_matl_class
  use htpc_solver_class
  !use htpc_mfd_solver_type
  !use htpc_fv_solver_type
  use htpc_ml_solver_type
  use flow_solver_type
  use enthalpy_advector_type
  use ustruc_model_type
  use amrex_base_module, only: amrex_multifab
  implicit none
  private

  type, public :: pbf_solver
    private
    type(truchas_env) :: env
    type(ml_mesh), pointer :: mesh => null() ! reference only -- not owned
    type(amrex_mesh), pointer :: fine_level => null() ! reference only -- not owned
    type(pbf_model), pointer :: model => null()  ! reference only -- not owned
    !class(htpc_solver), allocatable :: heat
    type(htpc_ml_solver), allocatable :: heat
    type(flow_solver) :: flow
    type(enthalpy_advector) :: hadv
    type(ustruc_model), allocatable :: ustruc  !TODO: better in pbf_simulation?
    real(r8) :: t
    logical :: state_is_pending = .false.
    logical, public :: flow_enabled
    type(amrex_multifab), allocatable :: temp(:), lfrac(:), grad(:)
    !! For state save/restore
    type(pbf_solver), pointer :: cache => null()
  contains
    procedure :: init
    procedure :: set_initial_state
    procedure :: set_initial_flow_state
    procedure :: start
    procedure :: step
    procedure :: commit_pending_state
    procedure :: time
    procedure :: write_metrics
    !procedure :: save_state
    !procedure :: restore_state
    !procedure :: get_bndry_heat_flux
    !procedure :: get_bndry_temp
    procedure :: get_temp
    procedure :: get_temp_grad
    procedure :: get_enth
    procedure :: get_vol_frac
    procedure :: get_liq_frac
    procedure :: max_liq_frac
    procedure :: get_velocity
    procedure :: get_pressure
    procedure :: has_exaca_data
    procedure :: get_exaca_data
    procedure :: has_gv_data
    procedure :: get_gv_data
    procedure :: get_exaca_state_size
    procedure :: get_exaca_state
    procedure :: set_exaca_state
    !! References to the current solution state
    procedure :: get_cell_temperature_view
    procedure :: get_cell_velocity_view
    procedure :: get_xface_velocity_view
    procedure :: get_yface_velocity_view
    procedure :: get_zface_velocity_view
    procedure :: get_cell_pressure_view
#ifdef NAG_SR104916
    final :: pbf_solver_delete
#endif
  end type pbf_solver

contains

#ifdef NAG_SR104916
  !! NAG finalizes allocatables last, which can cause multifabs to be finalized after amrex.
  subroutine pbf_solver_delete(this)
    type(pbf_solver), intent(inout) :: this
    if (allocated(this%heat)) deallocate(this%heat)
    if (allocated(this%temp)) deallocate(this%temp)
    if (allocated(this%lfrac)) deallocate(this%lfrac)
    if (allocated(this%grad)) deallocate(this%grad)
  end subroutine
#endif

  subroutine init(this, env, model, params)

    use parameter_list_type
    use pbf_matl_model_type

    class(pbf_solver), intent(out) :: this
    type(truchas_env), intent(in) :: env
    type(pbf_model), intent(in), target :: model
    type(parameter_list), intent(inout) :: params

    type(parameter_list), pointer :: plist
    integer :: l, stat
    character(:), allocatable :: errmsg

    this%env = env
    this%model => model
    this%mesh => model%mesh
    this%fine_level => this%mesh%level(this%mesh%max_level)

    if (params%is_sublist('heat')) then
      plist => params%sublist('heat')
      ! select case (this%model%discretization)
      ! case ("finite-volume")
      !   allocate(htpc_fv_solver :: this%heat)
      ! case ("mimetic")
      !   allocate(htpc_mfd_solver :: this%heat)
      ! case default
      !   stat = 1
      !   errmsg = "Heat model discretization must be either 'finite-volume' or 'mimetic'"
      !   return
      ! end select
      allocate(this%heat)
      call this%heat%init(this%env, this%model%heat, plist)
    else
      call this%env%log%fatal('missing "heat" sublist parameter')
    end if

    allocate(this%temp(0:this%mesh%max_level), this%lfrac(0:this%mesh%max_level), this%grad(0:this%mesh%max_level))
    call this%mesh%ml_multifab_build(this%temp, nc=1, ng=1)  ! flow wants a ghost
    call this%mesh%ml_multifab_build(this%lfrac, nc=1, ng=1)
    do l = 0, this%mesh%max_level
      call this%temp(l)%setval(0.0_r8)
      call this%lfrac(l)%setval(0.0_r8)
    end do

    !! TODO: The microstructure package is currently only on the finest level.
    if (params%is_sublist('microstructure')) then
      allocate(this%ustruc)
      plist => params%sublist('microstructure')
      call this%ustruc%init(this%fine_level, plist, stat, errmsg)
      if (stat /= 0) then
        call this%env%log%fatal('error initializing microstructure analysis: ' // errmsg)
      end if
      call this%mesh%ml_multifab_build(this%grad, nc=3, ng=0)
    end if

    this%flow_enabled = this%model%flow_enabled
    if (.not.this%flow_enabled) return

    if (params%is_sublist('flow')) then
      plist => params%sublist('flow')
      call this%flow%init(this%env, this%model%flow, plist)
    else
      call this%env%log%fatal('missing "flow" sublist parameter')
    end if

    call this%hadv%init(this%model%heat%mat(this%mesh%max_level), this%flow%vof)

  end subroutine init

  subroutine set_initial_state(this, t, temp)

    class(pbf_solver), intent(inout) :: this
    real(r8), intent(in) :: t
    type(amrex_multifab), intent(in) :: temp(0:)

    integer :: l
    type(amrex_multifab) :: cell_vel, face_vel(3)

    !! NB: ghosts are copied because finer-level ghosts along
    !!     level-edges are not set by fill-boundary.
    this%t = t
    do l = 0, this%mesh%max_level
      call this%temp(l)%copy(temp(l), 1, 1, 1, 1)
      call this%temp(l)%fill_boundary(this%mesh%geom(l))
    end do

    call this%heat%set_initial_state(t, this%temp)
    call this%heat%get_liq_frac(this%lfrac,1)
    if (this%flow_enabled) then
      call this%fine_level%multifab_build(cell_vel, nc=3, ng=0)
      call this%fine_level%multifab_build(face_vel(1), nc=1, ng=0, nodal=XFACE_NODAL)
      call this%fine_level%multifab_build(face_vel(2), nc=1, ng=0, nodal=YFACE_NODAL)
      call this%fine_level%multifab_build(face_vel(3), nc=1, ng=0, nodal=ZFACE_NODAL)
      call cell_vel%setval(0.0_r8)
      call face_vel(1)%setval(0.0_r8)
      call face_vel(2)%setval(0.0_r8)
      call face_vel(3)%setval(0.0_r8)
      call this%flow%set_initial_state(this%temp(this%mesh%max_level), this%lfrac(this%mesh%max_level), cell_vel, face_vel)
    end if
    if (allocated(this%ustruc)) then
      call this%heat%get_temp_soln(this%temp, 1)
      call this%heat%get_temp_grad(this%grad)
      call this%ustruc%set_state(t, this%temp(this%mesh%max_level), this%grad(this%mesh%max_level), this%lfrac(this%mesh%max_level))
    end if

  end subroutine set_initial_state

  subroutine set_initial_flow_state(this, vcell, vface, press)
    class(pbf_solver), intent(inout) :: this
    type(amrex_multifab), intent(in) :: vcell, vface(:), press
    INSIST(this%flow_enabled)
    call this%flow%set_initial_state_restart(this%t, this%temp(this%mesh%max_level), &
        this%lfrac(this%mesh%max_level), vcell, vface, press)
  end subroutine

  !! Start/restart the solver
  subroutine start(this, dt, explicit_ht)
    class(pbf_solver), intent(inout) :: this
    real(r8), intent(in) :: dt
    logical, intent(in) :: explicit_ht
    call this%heat%start(dt, explicit_ht)
  end subroutine start

  subroutine step(this, t, hnext, stat)

    class(pbf_solver), intent(inout) :: this
    real(r8), intent(in)  :: t
    real(r8), intent(out) :: hnext
    integer,  intent(out) :: stat

    real(r8) :: tlast
    type(amrex_multifab) :: fluid_rho_n, lfrac_n

    tlast = this%heat%time()

    if (this%flow_enabled) then
      call this%fine_level%multifab_build(fluid_rho_n, nc=1, ng=1)
      call fluid_rho_n%copy(this%flow%fluid_rho, 1, 1, 1, 1)  !TODO: move into flow

      call this%fine_level%multifab_build(lfrac_n, nc=1, ng=1)
      call lfrac_n%copy(this%lfrac(this%mesh%max_level), 1, 1, 1, 1)
      call this%heat%get_liq_frac(this%lfrac, 1)
      call this%heat%get_temp_soln(this%temp, 1)

      call this%flow%vol_advect_step(t-tlast, this%lfrac(this%mesh%max_level))
      call this%hadv%get_source_density(this%temp(this%mesh%max_level), this%model%heat%level(this%mesh%max_level)%qmf)
    end if

    call this%heat%step(t, hnext, stat)
    if (stat /= 0) return
    call this%heat%get_liq_frac(this%lfrac, 1)

    if (this%flow_enabled) then
      call this%heat%get_temp_soln(this%temp, 1)
      call this%flow%step(t-tlast, tlast, this%temp(this%mesh%max_level), lfrac_n, this%lfrac(this%mesh%max_level), fluid_rho_n, stat)
      if (stat /= 0) then ! no recovery attempted here yet
        stat = -2
        return
      end if
      hnext = min(hnext, this%flow%timestep_limit())
    end if

    !TODO: Do we need to manually destroy the local multifabs (for gfortran)?

  end subroutine step

  subroutine commit_pending_state(this)
    class(pbf_solver), intent(inout) :: this
    call this%heat%commit_pending_state
    if (this%flow_enabled) then
      call this%flow%commit_pending_state
      call this%flow%model%update_viscosity(this%temp(this%mesh%max_level), this%lfrac(this%mesh%max_level))  !TODO: belongs in flow
    end if
    if (allocated(this%ustruc)) then
      call this%heat%get_temp_soln(this%temp, 1)
      call this%heat%get_temp_grad(this%grad)
      call this%ustruc%update_state(this%heat%time(), this%temp(this%mesh%max_level), this%grad(this%mesh%max_level), this%lfrac(this%mesh%max_level))
    end if
  end subroutine commit_pending_state

  !! Current solution time. Get it from the heat transfer solver.
  !TODO: This ought to be maintained by this object.
  real(r8) function time(this)
    class(pbf_solver), intent(in) :: this
    time = this%heat%time()
  end function time

  !! Get the temperature solution at the last accepted step. The solution is
  !! copied into component N of the given multifab DEST. At this point the heat
  !! transfer solver holds the official value. The current implementation will
  !! fill ghost cell values, if any.

  subroutine get_temp(this, dest, n)
    class(pbf_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: dest(0:)
    integer, intent(in) :: n
    call this%heat%get_temp_soln(dest, n)
  end subroutine get_temp

  !! Get the gradient of the temperature solution at the last accepted step.
  !! The gradient is copied into component N of the given multifab DEST.
  !! Ghost cell values, if any, are not set.

  subroutine get_temp_grad(this, grad)
    class(pbf_solver), intent(inout) :: this
    type(amrex_multifab), intent(inout) :: grad(0:)
    call this%heat%get_temp_grad(grad)
  end subroutine get_temp_grad

  !! Get the enthalpy solution at the last accepted step. The solution is
  !! copied into component N of the given multifab DEST. The returned enthalpy
  !! is currently the approximate solution obtained from the heat transfer
  !! solver; approximate because the algebraic temperature/enthalpy relation
  !! is not exactly solved. A better alternative may be to compute it from the
  !! temperature solution. The current implementation will fill ghost cell
  !! values, if any.

  subroutine get_enth(this, dest, n)
    class(pbf_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: dest(0:)
    integer, intent(in) :: n
    call this%heat%get_enth_soln(dest, n)
  end subroutine get_enth

  !! Get the solid and liquid volume fractions at the last accepted step. The
  !! two volume fractions are copied into the given multifab DEST starting at
  !! component N.
  !! TODO: Get this data from the material model

  subroutine get_vol_frac(this, dest, n)
    class(pbf_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: dest(0:)
    integer, intent(in) :: n
    call this%heat%get_vol_frac(dest, n)
  end subroutine get_vol_frac

  !! Get the liquid volume fractions at the last accepted step. The volume
  !! fractions are copied into component N of the given multifab DEST.
  !! TODO: Get this data from the material model

  subroutine get_liq_frac(this, dest, n)
    class(pbf_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: dest(0:)
    integer, intent(in) :: n
    call this%heat%get_liq_frac(dest, n)
  end subroutine get_liq_frac

  !! Get the maximum liquid volume fraction at the last successful step.
  function max_liq_frac(this) result(maxlf)
    class(pbf_solver), intent(in) :: this
    real(r8) :: maxlf
    maxlf = this%lfrac(this%mesh%max_level)%max(1)
  end function

  subroutine get_velocity(this, dest, n)
    class(pbf_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: dest
    integer, intent(in) :: n
    if (this%flow_enabled) call this%flow%get_velocity(dest, n)
  end subroutine get_velocity

  subroutine get_pressure(this, dest, n)
    class(pbf_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: dest
    integer, intent(in) :: n
    if (this%flow_enabled) call this%flow%get_pressure(dest, n)
  end subroutine get_pressure

  subroutine write_metrics(this)
    class(pbf_solver), intent(in) :: this
    character(80) :: string(2)
    real(r8) :: vmax(3), vmin(3)
    integer :: k
    if (.not.this%heat%explicit) then
      call this%heat%write_metrics(string)
      call this%env%log%info('Implicit HT: ' // string(2))
    end if
    if (this%flow_enabled) then
      if (max_liq_frac(this) > 0.0_r8) then
        do k = 1, 3
          vmin(k) = this%flow%velocity%min(k)
          vmax(k) = this%flow%velocity%max(k)
        end do
        write(string(1),'("min velocity = (",es11.4,2(",",es12.4),")")') vmin
        write(string(2),'("max velocity = (",es11.4,2(",",es12.4),")")') vmax
        call this%env%log%info(string(1))
        call this%env%log%info(string(2))
      end if
    end if
  end subroutine write_metrics

  logical function has_exaca_data(this)
    class(pbf_solver), intent(in) :: this
    has_exaca_data = .false.
    if (allocated(this%ustruc)) has_exaca_data = this%ustruc%has('melting-time')
  end function has_exaca_data

  subroutine get_exaca_data(this, mf, n)
    class(pbf_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: mf
    integer, intent(in) :: n
    INSIST(has_exaca_data(this))
    call this%ustruc%get('melting-time', mf, n)
    call this%ustruc%get('freezing-time', mf, n+1)
    call this%ustruc%get('cooling-rate', mf, n+2)
  end subroutine get_exaca_data

  subroutine get_exaca_state_size(this, nbyte, ncell)
    class(pbf_solver), intent(in) :: this
    integer, intent(out) :: nbyte, ncell
    call this%ustruc%get_state_size(3, nbyte, ncell)  ! 3 is the exaca model ID
  end subroutine

  subroutine get_exaca_state(this, state)
    use,intrinsic :: iso_fortran_env, only: int8
    class(pbf_solver), intent(in) :: this
    integer(int8), intent(out) :: state(:,:)
    call this%ustruc%serialize(3, state)  ! 3 is the exaca model ID
  end subroutine

  subroutine set_exaca_state(this, state, mask)
    use,intrinsic :: iso_fortran_env, only: int8
    class(pbf_solver), intent(inout) :: this
    integer(int8), intent(in) :: state(:,:)
    logical, intent(in), optional :: mask(:)
    call this%ustruc%deserialize(3, state, mask)  ! 3 is the exaca model ID
  end subroutine

  logical function has_gv_data(this)
    class(pbf_solver), intent(in) :: this
    has_gv_data = .false.
    if (allocated(this%ustruc)) has_gv_data = this%ustruc%has('g')
  end function has_gv_data

  subroutine get_gv_data(this, mf, n)
    class(pbf_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: mf
    integer, intent(in) :: n
    INSIST(has_gv_data(this))
    call this%ustruc%get('g', mf, n)
    call this%ustruc%get('v', mf, n+1)
    call this%ustruc%get('e', mf, n+2, 3)
    call this%ustruc%get('solid-time', mf, n+5)
  end subroutine get_gv_data

  !! These subroutines return references to internal multifabs holding the
  !! current solution state. CONSIDER THESE MULTIFAB REFERENCES READ-ONLY!
  !! The FLOW_SOLVER object must have the TARGET attribute in the caller.

  subroutine get_cell_temperature_view(this, view)
    class(pbf_solver), intent(inout), target :: this
    type(amrex_multifab), pointer, intent(out) :: view(:)
    if (.not.this%flow_enabled) call this%heat%get_temp_soln(this%temp, 1)
    view => this%temp
  end subroutine get_cell_temperature_view

  subroutine get_cell_velocity_view(this, view)
    class(pbf_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    call this%flow%get_cell_velocity_view(view)
  end subroutine get_cell_velocity_view

  subroutine get_xface_velocity_view(this, view)
    class(pbf_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    call this%flow%get_xface_velocity_view(view)
  end subroutine get_xface_velocity_view

  subroutine get_yface_velocity_view(this, view)
    class(pbf_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    call this%flow%get_yface_velocity_view(view)
  end subroutine get_yface_velocity_view

  subroutine get_zface_velocity_view(this, view)
    class(pbf_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    call this%flow%get_zface_velocity_view(view)
  end subroutine get_zface_velocity_view

  subroutine get_cell_pressure_view(this, view)
    class(pbf_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    call this%flow%get_cell_pressure_view(view)
  end subroutine get_cell_pressure_view

end module pbf_solver_type
