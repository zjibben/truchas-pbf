//
// C-PINT1
//
// This is C version of PINT1.F90 which exercises most of the basic functions
// of the parallel-in-time interface to Truchas-PBF. A focus here is to verify
// concurrent independent simulations on disjoint MPI communicators and repeated
// creation and destruction of solver objects on the same communicator. This
// problem is based on the spot weld example, but with flow disabled.
//
// It starts with 2 independent, concurrent, asynchronous simulations, one on
// on the odd MPI ranks and the other on the even MPI ranks. These are the "A"
// and "B" simulations. They use the same input file but a different constant
// initial temperature. Half way through the simulation (T1) we capture the
// solution, and then continue on to the end (T2).
//
// It then runs a second pair of independent simulations, but these start
// at T1 and use the captured solutions from the first pair as the initial
// temperature, and they run to the same end (T2). The final solutions for
// A1 and A2 should be the same. Likewise for B1 and B2. Not exactly because
// the second simulations are starting fresh with different time steps, but
// they should be reasonably close.
//
// Neil N. Carlson <nnc@lanl.gov>
// June 2019
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tpbf-pint.h"

int main(int argc, char **argv)
{
  int ierr, this_rank, nproc, this_sim, sim_rank;
  MPI_Comm comm;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &this_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  if (nproc < 2) {
    fprintf(stderr, "ERROR: must run with at least 2 MPI ranks\n");
    MPI_Finalize();
    return 1;
  }

  this_sim = this_rank % 2;
  MPI_Comm_split(MPI_COMM_WORLD, this_sim, this_rank, &comm);
  MPI_Comm_rank(comm, &sim_rank);

  double temp0;
  char *infile, *outdir1, *outdir2;
  if (this_sim == 0) {
    infile  = "pint1.json";
    temp0   = 300.0;
    outdir1 = "outA1";
    outdir2 = "outA2";
  } else {
    infile  = "pint1.json";
    temp0   = 400.0;
    outdir1 = "outB1";
    outdir2 = "outB2";
  }

  double t0 = 0;
  double t1 = 0.01;
  double t2 = 0.02;

  void *solver;
  int ncell, nxface, nyface, nzface;

  // First simulation pair

  solver = tpbf_create_solver(comm, infile, outdir1);
  if (!solver) goto sim1_exit;

  tpbf_get_mesh_sizes(solver, &ncell, &nxface, &nyface, &nzface);
  double *temp1 = (double*) malloc(sizeof(double)*ncell);
  double *temp2 = (double*) malloc(sizeof(double)*ncell);
  for (int j = 0; j < ncell; j++) temp1[j] = temp0;
  tpbf_set_initial_state_ht(solver, t0, ncell, temp1);
  tpbf_write_solution(solver, 0);

  ierr = tpbf_integrate_to(solver, t1);
  if (ierr != 0) goto sim1_exit;
  tpbf_write_solution(solver, 1);

  tpbf_get_current_state_ht(solver, ncell, temp2);

  ierr = tpbf_integrate_to(solver, t2);
  if (ierr != 0) goto sim1_exit;
  tpbf_write_solution(solver, 2);

  tpbf_get_current_state_ht(solver, ncell, temp1);

  tpbf_destroy_solver(solver);

  sim1_exit: if (ierr != 0) {
    if (sim_rank == 0)
        fprintf(stderr,"Simulation failed; see %s/sim.log for details\n", outdir1);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 1;
  }

  // Second simulation pair

  solver = tpbf_create_solver(comm, infile, outdir2);
  if (!solver) goto sim2_exit;

  tpbf_set_initial_state_ht(solver, t1, ncell, temp2);
  tpbf_write_solution(solver, 1);

  ierr = tpbf_integrate_to(solver, t2);
  if (ierr != 0) goto sim2_exit;
  tpbf_write_solution(solver, 2);

  tpbf_get_current_state_ht(solver, ncell, temp2);

  tpbf_destroy_solver(solver);

  sim2_exit: if (ierr != 0) {
    if (sim_rank == 0)
        fprintf(stderr,"Simulation failed; see %s/sim.log for details\n", outdir2);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 1;
  }

  // Compute the error

  double relerr = 0.0, tmp;
  for (int j=0; j < ncell; j++) {
    tmp = fabs((temp2[j] - temp1[j])/temp1[j]);
    if (tmp > relerr) relerr = tmp;
  }
  MPI_Allreduce(MPI_IN_PLACE, &relerr, 1, MPI_DOUBLE, MPI_MAX, comm);
  if (sim_rank == 0) printf("%s: max rel temp error = %13.5e\n", outdir2, relerr);
  if (relerr > 5.0e-5) {
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 2;
  }

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
