// Parallel-in-Time Library Interface to Truchas-PBF
//
// Neil N. Carlson <nnc@lanl.gov>
// June 2019

#include "mpi.h"

void* tpbf_create_solver(MPI_Comm comm, char *infile, char *outdir);
// Create a new Truchas-PBF solver instance.  The function returns an opaque
// pointer to the instance, which is passed to the other functions in the API.
// A null pointer indicates an error. Comm is the MPI communicator the solver
// should use (not MPI_COMM_NULL). All other functions are collective across
// the processes in the communicator. Infile is the path to the Truchas-PBF
// input file, or the content of the JSON-format input file as a string. The
// later case is assumed when the first character of infile is '{'; this is
// extremely unlikely to conflict with an actual file name. The input file
// defines nearly all parameters for the solver. All output files will be
// placed in the directory outdir, which will be created if it does not exist.

int tpbf_destroy_solver(void* solver);
// Finalize the Truchas-PBF solver instance.

int tpbf_integrate_to(void *solver, double t);
// Integrates the system forward from its current state to time T. A non-zero
// return value indicates a failure to successfully integrate to T. The initial
// state must be set before calling this function.

void tpbf_write_solution(void *solver, int seqno);
// Write the current solution state to an AMReX plot file. The plot file is
// created in the specified output directory and named pltnnnn, where nnnn is
// the value of seqno with leading zeros.

void tpbf_disable_laser(void *solver);
// By default the laser is enabled and is either on or off as controlled by
// the scan path. This call will disable the laser so that it is always off
// irrespective of the scan path. Note that there is no call to enable the
// laser.

int tpbf_is_laser_enabled(void *solver);
// Return the status of the laser: 1 for enabled, 0 for disabled.


// The following functions are involved in setting and getting the state
// variable arrays for the heat transfer/fluid flow system.


void tpbf_get_ht_mesh_sizes(void *solver, int *ncell);
// Gets the sizes of the heat transfer state variable arrays. These are
// related to the underlying AMReX mesh, but for the purposes of this
// interface they are merely the sizes of those arrays and corresponding
// coordinate arrays.

void tpbf_get_ht_cell_centers(void *solver, int ncell, double *coord);
// Get the coordinates of the heat transfer mesh cell centers. The coordinates
// are returned in the format (x0,y0,z0,x1,y1,z1,...). The size of coord must
// be at least 3*ncell.

void tpbf_set_initial_state(void *solver, double t, int ncell, double *temp);
// Set the initial time and value of the state variables. Only cell-centered
// temperatures on the heat transfer mesh are specified. If flow is enabled,
// the flow state variables are set to zero. Call tpbf_set_initial_flow_state
// afterwards to set the flow state variables to different values.

// Zach: if there is fluid initially, how do we set the pressure? Should we /
// do we solve for the initial pressure?

void tpbf_get_current_ht_state(void *solver, int ncell, double *temp);
// Get the current value of the heat transfer state variables.

void tpbf_get_flow_mesh_sizes(void *solver, int *ncell, int *nxface, int *nyface, int *nzface);
// Gets the sizes of the flow state variable arrays. These are related to the
// underlying AMReX mesh, but for the purposes of this interface they are merely
// the sizes of those arrays.

void tpbf_get_flow_cell_centers (void *solver, int ncell,  double *coord);
void tpbf_get_flow_xface_centers(void *solver, int nxface, double *coord);
void tpbf_get_flow_yface_centers(void *solver, int nyface, double *coord);
void tpbf_get_flow_zface_centers(void *solver, int nzface, double *coord);
// Get the coordinates of the cell centers and the x/y/z-normal face centers
// for the flow mesh. The coordinates are returned in the format (x0,y0,z0,
// x1,y1,z1,...). The size of coord for cell centers, for example, must be
// at least 3*ncell.

void tpbf_get_current_flow_state(void *solver, int ncell, double *vcell, double *press,
    int nxface, double *vxface, int nyface, double *vyface, int nzface, double *vzface);
// Get the current value of the flow state variables. Flow must be enabled.
// Data corresponds to the flow mesh coordinate arrays.

void tpbf_set_initial_flow_state(void *solver, int ncell, double *vcell, double *press,
    int nxface, double *vxface, int nyface, double *vyface, int nzface, double *vzface);
// Set the initial value of the flow state variables. This must be called
// after a call to tpbf_set_initial_state to set the initial time and
// heat transfer state. This can only be used when flow is enabled, and
// even then it is optional. To ensure the flow variables are consistent
// with each other, they should have been obtained from an earlier call to
// tpbf_get_current_flow_state.


// The following functions deal with the solidificaton state date collected
// during a simulation. This data is opaque and intended only to be passed
// between successive simulations, much like the temperature and velocity
// state data.

void tpbf_get_exaca_state_size(void *solver, int *nbyte, int *ncell);
// Get the size of the exaca state data. The size of the int8_t state array is
// nbyte*ncell. Note that the value of ncell here is not necessarily the same
// as for the mesh.

void tpbf_get_exaca_state(void *solver, int nbyte, int ncell, int8_t *state);
// Get the ExaCA state data.

void tpbf_set_exaca_state(void *solver, int nbyte, int ncell, int8_t *state);
// Set the ExaCA state data. The state array should have been returned by the
// get function for a simulation using the same mesh and AMReX box size.

int tpbf_num_solid_cells(void *solver);
// Return the number of solidified cells at the current time. This is needed
// to ensure the array arguments to tpbf_get_solid_times are adequately sized.
// Note that the number of solidified cells will change over the course of the
// simulation.

void tpbf_get_solid_times(void *solver, int ncell, double *coord, double *tstart, double *tfinish);
// Get the current solidification start and stop times for each solidified
// cell and the corresponding cell centers. Here ncell is the size of the
// tstart and tfinish arrays, and the size of the coord array is 3*ncell.
// Ncell must be no smaller than the value returned by tpbf_num_solid_cells.

void tpbf_write_solid_times(void *solver, const char *filename);
// Write the solidification start/finish times to an AMReX plot file with the
// given filename. The plot file is created in the specified output directory.
