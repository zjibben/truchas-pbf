target_sources(truchas-pbf PRIVATE
    scan_path_type.F90
    xy_motion_class.F90
    dwell_xy_motion_type.F90
    linear_xy_motion_type.F90
    scan_path_factory.F90
    laser_irrad_class.F90
    laser_absorb_class.F90
    gauss_laser_irrad_type.F90
    gauss_laser_absorb_type.F90
    laser_irrad_factory.F90
    laser_absorb_factory.F90
    laser_scan_type.F90
)

if(ENABLE_TESTS)
  add_subdirectory(test)
endif()

