!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module flow_solver_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use amrex_mesh_type
  use amrex_base_module, only: amrex_multifab
  use predictor_solver_type
  use projection_solver_type
  use parameter_list_type
  use flow_model_class
  use vof_solver_type
  use truchas_env_type
  use timer_tree_type
  implicit none
  private

  type, public :: flow_solver
    type(truchas_env), pointer :: env => null()
    type(amrex_mesh), pointer :: mesh => null()
    class(flow_model), pointer :: model => null()
    type(parameter_list), pointer :: params => null()
    type(vof_solver) :: vof

    type(predictor_solver) :: predictor
    type(projection_solver) :: projection

#ifdef GNU_PR82996
    type(amrex_multifab), allocatable :: face_velocity(:), face_velocity_new(:), gradp_face(:), &
        gradp_face_new(:)
    type(amrex_multifab) :: velocity, pressure, gradp_dyn_rho, fluid_rho
    type(amrex_multifab) :: velocity_new, pressure_new, gradp_dyn_rho_new, fluid_rho_new
#else
    type(amrex_multifab) :: face_velocity(3), velocity, pressure, gradp_dyn_rho, fluid_rho, &
        gradp_face(3)
    type(amrex_multifab) :: face_velocity_new(3), velocity_new, pressure_new, gradp_dyn_rho_new, &
                            fluid_rho_new, gradp_face_new(3)
#endif

    real(r8) :: vimpl, cfl_factor
    logical :: viscous, skipped_step
    logical, allocatable :: fluid_in_box(:)
  contains
    procedure :: init
    procedure :: vol_advect_step
    procedure :: step
    procedure :: commit_pending_state
    procedure :: timestep_limit
    procedure :: set_initial_state
    procedure :: set_initial_state_restart
    procedure :: get_velocity
    procedure :: get_pressure
    procedure, private :: mirror_boundary_ghosts
    procedure, private :: clip_volfrac
    procedure, private :: find_fluid_boxes
    procedure, private :: update_fluid_rho
    procedure, private :: perform_flow
    !! References to the current solution state
    procedure :: get_cell_velocity_view
    procedure :: get_xface_velocity_view
    procedure :: get_yface_velocity_view
    procedure :: get_zface_velocity_view
    procedure :: get_cell_pressure_view
  end type flow_solver

  real(r8), parameter :: fluid_cutoff = 1e-2_r8

contains

  subroutine get_velocity(this, dest, n)
    class(flow_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: dest
    integer, intent(in) :: n
    call dest%copy(this%velocity, 1, n, 3, 0)
  end subroutine get_velocity

  subroutine get_pressure(this, dest, n)
    class(flow_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: dest
    integer, intent(in) :: n
    call dest%copy(this%pressure, 1, n, 1, 0)
  end subroutine get_pressure

  subroutine init(this, env, model, params)

    class(flow_solver), intent(out) :: this
    type(truchas_env), intent(in), target :: env
    class(flow_model), intent(in), target :: model
    type(parameter_list), intent(inout) :: params

    integer :: stat
    character(:), allocatable :: context, errmsg

    call start_timer('flow')

#ifdef GNU_PR82996
    allocate(this%face_velocity(3), this%face_velocity_new(3), &
        this%gradp_face(3), this%gradp_face_new(3))
#endif

    this%env => env
    this%model => model
    this%mesh => model%mesh

    allocate(this%fluid_in_box(size(this%mesh%box)))

    call this%vof%init(this%mesh, params)
    call this%projection%init(env, this%mesh, params)
    call this%predictor%init(env, this%mesh, params)

    !TODO: No need for face_velocity ghosts?
    call this%mesh%multifab_build(this%face_velocity(1), nc=1, ng=1, nodal=xface_nodal)
    call this%mesh%multifab_build(this%face_velocity(2), nc=1, ng=1, nodal=yface_nodal)
    call this%mesh%multifab_build(this%face_velocity(3), nc=1, ng=1, nodal=zface_nodal)
    call this%mesh%multifab_build(this%gradp_face(1), nc=1, ng=1, nodal=xface_nodal)
    call this%mesh%multifab_build(this%gradp_face(2), nc=1, ng=1, nodal=yface_nodal)
    call this%mesh%multifab_build(this%gradp_face(3), nc=1, ng=1, nodal=zface_nodal)
    call this%mesh%multifab_build(this%velocity, nc=3, ng=1)
    call this%mesh%multifab_build(this%pressure, nc=1, ng=1)
    call this%mesh%multifab_build(this%gradp_dyn_rho, nc=3, ng=1)
    call this%mesh%multifab_build(this%fluid_rho, nc=1, ng=1)

    call this%mesh%multifab_build(this%face_velocity_new(1), nc=1, ng=1, nodal=xface_nodal)
    call this%mesh%multifab_build(this%face_velocity_new(2), nc=1, ng=1, nodal=yface_nodal)
    call this%mesh%multifab_build(this%face_velocity_new(3), nc=1, ng=1, nodal=zface_nodal)
    call this%mesh%multifab_build(this%gradp_face_new(1), nc=1, ng=1, nodal=xface_nodal)
    call this%mesh%multifab_build(this%gradp_face_new(2), nc=1, ng=1, nodal=yface_nodal)
    call this%mesh%multifab_build(this%gradp_face_new(3), nc=1, ng=1, nodal=zface_nodal)
    call this%mesh%multifab_build(this%velocity_new, nc=3, ng=1)
    call this%mesh%multifab_build(this%pressure_new, nc=1, ng=1)
    call this%mesh%multifab_build(this%gradp_dyn_rho_new, nc=3, ng=1)
    call this%mesh%multifab_build(this%fluid_rho_new, nc=1, ng=1)

    ! multifabs with ghosts get set to 0 to define the domain ghost values
    call this%face_velocity(1)%setval(0.0_r8)
    call this%face_velocity(2)%setval(0.0_r8)
    call this%face_velocity(3)%setval(0.0_r8)
    call this%gradp_face(1)%setval(0.0_r8)
    call this%gradp_face(2)%setval(0.0_r8)
    call this%gradp_face(3)%setval(0.0_r8)
    call this%velocity%setval(0.0_r8)
    call this%pressure%setval(0.0_r8)
    call this%gradp_dyn_rho%setval(0.0_r8)
    call this%fluid_rho%setval(0.0_r8)

    ! parse input parameter list
    context = 'processing ' // params%name() // ': '

    this%viscous = .not.this%model%inviscid

    if (this%viscous .and. params%is_scalar('viscous-implicitness')) then
      call params%get('viscous-implicitness', this%vimpl, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
    else
      this%vimpl = 0
    end if

    if (params%is_scalar('courant-number')) then
      call params%get('courant-number', this%cfl_factor, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
    else
      this%cfl_factor = 0.9_r8
    end if

    call stop_timer('flow')

  end subroutine init

  !! Use this when you only have the externally visible state variables, and
  !! additional internal state variables that are consistent with it need to
  !! be computed.
  !! TODO: the face velocities should not be input, but computed/approximated.
  !! FIXME: t and dt should be passed in; hardwired t=0 dt=1 now.

  subroutine set_initial_state(this, temp, vol_frac, cell_vel, face_vel)

    class(flow_solver), intent(inout) :: this
    type(amrex_multifab), intent(in) :: temp, vol_frac, cell_vel, face_vel(:)

    integer :: n, ierr
    type(amrex_multifab) :: velocity, face_velocity(3), lfrac

    call start_timer('flow')

    !! The face velocity ghosts are used by the volume tracker,
    !! which needs to solve cell-outward volume fluxes in lower-left
    !! ghost cells, because faces on the lower left will not be
    !! updated from neighboring boxes. The ghost faces need only
    !! exist and hold valid floating-point data.
    do n = 1, 3
      call this%face_velocity(n)%setval(0.0_r8) !! set ghosts to zero
      call this%face_velocity(n)%copy(face_vel(n), 1, 1, 1, 0)
    end do

    !! Not filling cell velocity ghosts since overwritten later
    call this%velocity%copy(cell_vel, 1, 1, 3, 0)
    call this%velocity%fill_boundary(this%mesh%geom)

    ! initialize density
    call this%mesh%multifab_build(lfrac, nc=1, ng=1)
    call lfrac%copy(vol_frac, 1, 1, 1, 1)  ! copy to apply BCs and clip
    call this%mirror_boundary_ghosts(lfrac)
    call this%clip_volfrac(lfrac)
    call this%update_fluid_rho(lfrac)
    call this%model%update_viscosity(temp, lfrac)

    ! initialize pressure using pressure poisson in projection
    ! create temprorary copies of velocity
    call this%mesh%multifab_build(face_velocity(1), nc=1, ng=1, nodal=xface_nodal)
    call this%mesh%multifab_build(face_velocity(2), nc=1, ng=1, nodal=yface_nodal)
    call this%mesh%multifab_build(face_velocity(3), nc=1, ng=1, nodal=zface_nodal)
    call this%mesh%multifab_build(velocity, nc=3, ng=1)

    call this%pressure%setval(0.0_r8)
    call this%gradp_dyn_rho%setval(0.0_r8)
    call this%gradp_face(1)%setval(0.0_r8)
    call this%gradp_face(2)%setval(0.0_r8)
    call this%gradp_face(3)%setval(0.0_r8)
    if (this%perform_flow(lfrac)) then
      call face_velocity(1)%copy(this%face_velocity(1), 1, 1, 1, 1)
      call face_velocity(2)%copy(this%face_velocity(2), 1, 1, 1, 1)
      call face_velocity(3)%copy(this%face_velocity(3), 1, 1, 1, 1)
      call velocity%copy(this%velocity, 1, 1, 3, 1)

      call this%find_fluid_boxes(lfrac)

      ! this is a hack to apply BC information to the face pressure gradient
      call this%projection%compute_gradp_dyn_rho(0.0_r8, this%fluid_in_box, &
          this%model%body_force_density, this%model%density, this%model, this%fluid_rho, lfrac, &
          temp, this%pressure, this%gradp_dyn_rho, this%gradp_face)
      call this%gradp_dyn_rho%setval(0.0_r8)

      call this%projection%solve(1.0_r8, 0.0_r8, this%fluid_in_box, this%model%body_force_density, &
          this%model%density, this%model, this%fluid_rho, lfrac, temp, &
          velocity, this%pressure, face_velocity, this%gradp_dyn_rho, this%gradp_face, ierr)
      INSIST(ierr == 0)
    end if

    call stop_timer('flow')

  end subroutine set_initial_state

  !! Use this when you have a complete set of consistent state variables that
  !! were obtained from the flow solver.

  subroutine set_initial_state_restart(this, t, temp, vol_frac, cell_vel, face_vel, press)

    class(flow_solver), intent(inout) :: this
    real(r8), intent(in) :: t
    type(amrex_multifab), intent(in) :: cell_vel, face_vel(:), press, temp, vol_frac

    integer :: n
    type(amrex_multifab) :: lfrac

    call this%velocity%copy(cell_vel, 1, 1, 3, 0)
    call this%velocity%fill_boundary(this%mesh%geom)

    do n = 1, 3
      call this%face_velocity(n)%copy(face_vel(n), 1, 1, 1, 0)
    end do

    call this%pressure%copy(press, 1, 1, 1, 0)
    call this%pressure%fill_boundary(this%mesh%geom)

    call this%mesh%multifab_build(lfrac, nc=1, ng=1)
    call lfrac%copy(vol_frac, 1, 1, 1, 1) ! copy to apply BCs and clip
    call this%mirror_boundary_ghosts(lfrac)
    call this%clip_volfrac(lfrac)
    call this%update_fluid_rho(lfrac)
    call this%find_fluid_boxes(lfrac)
    call this%projection%compute_gradp_dyn_rho(t, this%fluid_in_box, &
          this%model%body_force_density, this%model%density, this%model, this%fluid_rho, &
          lfrac, temp, this%pressure, this%gradp_dyn_rho, this%gradp_face)

  end subroutine set_initial_state_restart


  subroutine update_fluid_rho(this, vol_frac)

    use amrex_base_module, only: amrex_mfiter, amrex_box

    class(flow_solver), intent(inout) :: this
    type(amrex_multifab), intent(in) :: vol_frac

    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx
    real(r8), pointer :: fluid_rho(:,:,:,:), volfrac(:,:,:,:)
    integer :: ix, iy, iz

    call this%mesh%mfiter_build(mfi)
    do while (mfi%next())
      bx = mfi%tilebox()
      volfrac => vol_frac%dataptr(mfi)
      fluid_rho => this%fluid_rho%dataptr(mfi)

      do iz = bx%lo(3),bx%hi(3)
        do iy = bx%lo(2),bx%hi(2)
          do ix = bx%lo(1),bx%hi(1)
            fluid_rho(ix,iy,iz,1) = merge(this%model%density, 0.0_r8, volfrac(ix,iy,iz,1) > 0)
          end do
        end do
      end do
    end do

    call this%mirror_boundary_ghosts(this%fluid_rho)
    call this%fluid_rho%fill_boundary(this%mesh%geom)

  end subroutine update_fluid_rho


  subroutine vol_advect_step(this, dt, volfrac)
    class(flow_solver), intent(inout) :: this
    real(r8), intent(in) :: dt
    type(amrex_multifab), intent(in) :: volfrac
    call start_timer('flow')
    call this%find_fluid_boxes(volfrac)
    call this%vof%compute_volume_flux(dt, this%fluid_in_box, this%face_velocity, volfrac)
    call stop_timer('flow')
  end subroutine vol_advect_step


  subroutine step(this, dt, t, temp_in, volfrac_n, volfrac, fluid_rho_n, ierr)

    class(flow_solver), intent(inout) :: this
    real(r8), intent(in) :: dt, t
    type(amrex_multifab), intent(in) :: temp_in, volfrac_n, volfrac, fluid_rho_n
    integer, intent(out) :: ierr

    integer :: n
    type(amrex_multifab) :: temp, lfrac

    call start_timer('flow')
    ierr = 0

    ! make a copy of the temperature that we can modify its domain ghosts for ST.
    call this%mesh%multifab_build(temp, nc=1, ng=1)
    call temp%copy(temp_in, 1, 1, 1, 1)

    ! get new density
    call this%mesh%multifab_build(lfrac, nc=1, ng=1)
    call lfrac%copy(volfrac, 1, 1, 1, 1) ! copy to apply BCs and clip
    call this%mirror_boundary_ghosts(lfrac)
    call this%clip_volfrac(lfrac)
    call this%update_fluid_rho(lfrac)

    ! skip flow if there is nothing to do
    this%skipped_step = .not.this%perform_flow(lfrac)
    if (.not.this%skipped_step) then
      call this%find_fluid_boxes(lfrac)

      ! copy old state
      call this%velocity_new%copy(this%velocity, 1, 1, 3, 1)
      call this%pressure_new%copy(this%pressure, 1, 1, 1, 1)
      call this%gradp_dyn_rho_new%copy(this%gradp_dyn_rho, 1, 1, 3, 1)
      do n = 1, 3
        call this%face_velocity_new(n)%copy(this%face_velocity(n), 1, 1, 1, 1)
        call this%gradp_face_new(n)%copy(this%gradp_face(n), 1, 1, 1, 1)
      end do

      call this%model%update_viscosity(temp, lfrac)

      ! solve the flow system
      call this%predictor%solve(dt, t, this%fluid_in_box, this%model, this%vof, temp, &
          this%gradp_dyn_rho_new, this%fluid_rho, lfrac, fluid_rho_n, volfrac_n, &
          this%velocity_new, ierr)
      if (ierr == 0) &
          call this%projection%solve(dt, t, this%fluid_in_box, this%model%body_force_density, &
          this%model%density, this%model, this%fluid_rho, lfrac, temp, &
          this%velocity_new, this%pressure_new, this%face_velocity_new, this%gradp_dyn_rho_new, &
          this%gradp_face_new, ierr)
    end if

    call stop_timer('flow')

  end subroutine step


  ! This routine will mirror adjacent internal values to
  ! boundary ghosts, setting up for a Neumann condition.
  subroutine mirror_boundary_ghosts(this, scalar)

    use amrex_base_module, only: amrex_mfiter

    class(flow_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: scalar

    type(amrex_mfiter) :: mfi
    integer :: n
    real(r8), pointer, contiguous :: s(:,:,:) => null()

    call this%mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1
      call get_component_ptr(scalar%dataptr(mfi), 1, s)

      associate(bx => this%mesh%box(n))
        if (bx%lo_is_bndry(1)) then
          s(bx%lo(1)-1, bx%lo(2):bx%hi(2), bx%lo(3):bx%hi(3)) = &
              s(bx%lo(1), bx%lo(2):bx%hi(2), bx%lo(3):bx%hi(3))
        end if

        if (bx%hi_is_bndry(1)) then
          s(bx%hi(1)+1, bx%lo(2):bx%hi(2), bx%lo(3):bx%hi(3)) = &
              s(bx%hi(1), bx%lo(2):bx%hi(2), bx%lo(3):bx%hi(3))
        end if

        if (bx%lo_is_bndry(2)) then
          s(bx%lo(1):bx%hi(1), bx%lo(2)-1, bx%lo(3):bx%hi(3)) = &
              s(bx%lo(1):bx%hi(1), bx%lo(2), bx%lo(3):bx%hi(3))
        end if

        if (bx%hi_is_bndry(2)) then
          s(bx%lo(1):bx%hi(1), bx%hi(2)+1, bx%lo(3):bx%hi(3)) = &
              s(bx%lo(1):bx%hi(1), bx%hi(2), bx%lo(3):bx%hi(3))
        end if

        if (bx%lo_is_bndry(3)) then
          s(bx%lo(1):bx%hi(1), bx%lo(2):bx%hi(2), bx%lo(3)-1) = &
              s(bx%lo(1):bx%hi(1), bx%lo(2):bx%hi(2), bx%lo(3))
        end if

        if (bx%hi_is_bndry(3)) then
          s(bx%lo(1):bx%hi(1), bx%lo(2):bx%hi(2), bx%hi(3)+1) = &
              s(bx%lo(1):bx%hi(1), bx%lo(2):bx%hi(2), bx%hi(3))
        end if
      end associate
    end do

  end subroutine mirror_boundary_ghosts

  ! clip volume fractions below a threshold to avoid small values
  subroutine clip_volfrac(this, volfrac)

    use amrex_base_module, only: amrex_mfiter

    class(flow_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: volfrac

    type(amrex_mfiter) :: mfi
    integer :: n
    real(r8), pointer, contiguous :: vof(:,:,:) => null()

    call this%mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1
      call get_component_ptr(volfrac%dataptr(mfi), 1, vof)
      where (vof < fluid_cutoff) vof = 0
    end do

  end subroutine clip_volfrac

  ! if there are at least 10 purely liquid cells in the system (excluding ghost
  ! cells), we turn on flow.
  logical function perform_flow(this, volfrac)

    use mpi
    use amrex_base_module, only: amrex_mfiter

    class(flow_solver), intent(in) :: this
    type(amrex_multifab), intent(in) :: volfrac

    type(amrex_mfiter) :: mfi
    integer :: n, ncell, ncell_global, ierr
    real(r8), pointer, contiguous :: vof(:,:,:) => null()

    perform_flow = .false.
    ncell = 0

    call this%mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1
      call get_component_ptr(volfrac%dataptr(mfi), 1, vof)

      associate(bx => this%mesh%box(n))
        ! perform_flow = perform_flow .or. &
        !     any(vof(bx%lo(1):bx%hi(1), bx%lo(2):bx%hi(2), bx%lo(3):bx%hi(3)) > fluid_cutoff)
        ncell = ncell &
            + count(vof(bx%lo(1):bx%hi(1), bx%lo(2):bx%hi(2), bx%lo(3):bx%hi(3)) > fluid_cutoff)
      end associate

      !if (perform_flow) exit
    end do
    call MPI_Allreduce(ncell, ncell_global, 1, MPI_INTEGER, MPI_SUM, this%env%comm, ierr)
    INSIST(ierr == 0)

    perform_flow = ncell_global >= 10

  end function perform_flow

  subroutine get_component_ptr(dp, n, cdp)
    real(r8), pointer, contiguous, intent(in) :: dp(:,:,:,:)
    integer, intent(in) :: n
    real(r8), pointer, contiguous, intent(out) :: cdp(:,:,:)
    cdp(lbound(dp,1):,lbound(dp,2):,lbound(dp,3):) => dp(:,:,:,n)
  end subroutine get_component_ptr


  subroutine find_fluid_boxes(this, volfrac)

    use amrex_base_module, only: amrex_mfiter

    class(flow_solver), intent(inout) :: this
    type(amrex_multifab), intent(in) :: volfrac

    type(amrex_mfiter) :: mfi
    integer :: n
    real(r8), pointer, contiguous :: vof(:,:,:) => null()

    call this%mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1
      call get_component_ptr(volfrac%dataptr(mfi), 1, vof)

      associate(bx => this%mesh%box(n))
        this%fluid_in_box(n) = any(vof(bx%lo(1):bx%hi(1), bx%lo(2):bx%hi(2), bx%lo(3):bx%hi(3)) > 0)
      end associate
    end do

    !this%fluid_in_box = .true. ! OVERRIDE
    ! print '("flow skipping ", i6, " of ", i6, " boxes")', &
    !     count(.not.this%fluid_in_box), size(this%fluid_in_box)

  end subroutine find_fluid_boxes


  subroutine commit_pending_state(this)

    class(flow_solver), intent(inout) :: this

    integer :: n

    ! TODO: is there a way to move rather than copy?

    if (this%skipped_step) then
      ! if we skipped a step, the system is treated as entirely solid
      this%skipped_step = .false.
      call this%velocity%setval(0.0_r8)
      call this%pressure%setval(0.0_r8)
      call this%gradp_dyn_rho%setval(0.0_r8)
      do n = 1, 3
        call this%face_velocity(n)%setval(0.0_r8)
        call this%gradp_face(n)%setval(0.0_r8)
      end do
    else
      ! this%velocity = this%velocity_new
      ! this%pressure = this%pressure_new
      ! this%face_velocity = this%face_velocity_new
      ! this%gradp_dyn_rho = this%gradp_dyn_rho_new

      call this%velocity%copy(this%velocity_new, 1, 1, 3, 1)
      call this%pressure%copy(this%pressure_new, 1, 1, 1, 1)
      call this%gradp_dyn_rho%copy(this%gradp_dyn_rho_new, 1, 1, 3, 1)
      do n = 1, 3
        call this%face_velocity(n)%copy(this%face_velocity_new(n), 1, 1, 1, 1)
        call this%gradp_face(n)%copy(this%gradp_face_new(n), 1, 1, 1, 1)
      end do
    end if

  end subroutine commit_pending_state

  function timestep_limit(this) result(dt)

    class(flow_solver), intent(in) :: this
    real(r8) :: dt

    real(r8) :: max_vel

    max_vel = max(this%face_velocity(1)%norm0(1), this%face_velocity(2)%norm0(1), &
        this%face_velocity(3)%norm0(1))
    dt = this%cfl_factor*minval(this%mesh%geom%dx / (max_vel + epsilon(1.0_r8)))
    
    if (this%viscous .and. this%vimpl < 0.5_r8) &
        dt = min(dt, 0.2_r8 * minval(this%mesh%geom%dx**2) / (this%model%viscosity_mf%max(1) + epsilon(1.0_r8)))

  end function timestep_limit

  !! These subroutines return references to internal multifabs holding the
  !! current solution state. CONSIDER THESE MULTIFAB REFERENCES READ-ONLY!
  !! The FLOW_SOLVER object must have the TARGET attribute in the caller.

  subroutine get_cell_velocity_view(this, view)
    class(flow_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    view => this%velocity
  end subroutine get_cell_velocity_view

  subroutine get_xface_velocity_view(this, view)
    class(flow_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    view => this%face_velocity(1)
  end subroutine get_xface_velocity_view

  subroutine get_yface_velocity_view(this, view)
    class(flow_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    view => this%face_velocity(2)
  end subroutine get_yface_velocity_view

  subroutine get_zface_velocity_view(this, view)
    class(flow_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    view => this%face_velocity(3)
  end subroutine get_zface_velocity_view

  subroutine get_cell_pressure_view(this, view)
    class(flow_solver), intent(in), target :: this
    type(amrex_multifab), pointer, intent(out) :: view
    view => this%pressure
  end subroutine get_cell_pressure_view

end module flow_solver_type
