!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

program test_lid_driven_cavity

#ifdef NAGFOR
  use,intrinsic :: f90_unix, only: exit
#endif
  use,intrinsic :: iso_fortran_env, only: output_unit
  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use mpi
  use amrex_base_module
  use parameter_list_type
  use parameter_list_json
  use truchas_amrex_init_proc
  use amrex_mesh_type
  use simlog_type
  use truchas_env_type
  implicit none

  integer :: j, nproc, this_rank, ierr, status = 0
  type(parameter_list) :: params
  type(truchas_env), target :: env
  type(amrex_mesh) :: mesh
#if defined(__GFORTRAN__)
  procedure(amrex_finalize), pointer :: pp
#endif
  character(:), allocatable :: paramstr

  call truchas_amrex_init(params)

#if defined(__GFORTRAN__)
  pp => amrex_finalize
  call env%log%init(MPI_COMM_WORLD, [output_unit], verbosity=VERB_NOISY, finalize=pp)
#else
  call env%log%init(MPI_COMM_WORLD, [output_unit], verbosity=VERB_NOISY, finalize=amrex_finalize)
#endif

  call MPI_Comm_rank(MPI_COMM_WORLD, this_rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)
  env%comm = MPI_COMM_WORLD
  env%rank = this_rank

  call params%set('lo', [0,0,0])
  !call params%set('hi', [31,31,0])
  !call params%set('hi', [31,31,31])
  !call params%set('hi', [63,63,0])
  call params%set('hi', [127,127,0])
  call params%set('box-size', 16)
  !call params%set('box-size', 64)
  call params%set('prob-lo', [0.0_r8, 0.0_r8, 0.0_r8])
  call params%set('prob-hi', [1.0_r8, 1.0_r8, 1.0_r8])
  call mesh%init(params)

  do j = 0, nproc-1
    if (j == this_rank) then
      write(*,'("process ",i0," has ",i0," boxes")') j, mesh%nbox
    endif
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
  end do

  ! set input parameters
  if (this_rank == 0) print *, 'x-dir flow'
  paramstr = '{"viscous-implicitness": 0.5, "viscosity": 2.5e-3, "density": 1.0, "max dt": 1e-2, &
      &"model":{ "density": 1.0, "viscosity": 1e-2}, &
      &"bc":{ &
      &"vel side1":{ "type": "velocity", "sides": ["yhi"], &
      &              "xdata": 1.0, "ydata": 0.0, "zdata": 0.0}, &
      &"vel side2":{ "type": "velocity", "sides": ["xlo", "ylo", "xhi"], &
      &              "xdata": 0.0, "ydata": 0.0, "zdata": 0.0}, &
      &"sym sides":{ "type": "symmetry", "sides": ["zlo", "zhi"]}}}'
  call lid_driven_cavity_test(paramstr, 1)

  ! if (this_rank == 0) print *, 'y-dir flow'
  ! paramstr = '{"viscous-implicitness": 0.5, "viscosity": 1.0, "density": 1.0, "max dt": 5e-2, &
  !     &"bc":{ &
  !     &"vel sides":{ "type": "velocity", "sides": ["xlo", "zlo", "xhi", "zhi"], &
  !     &              "xdata": 0.0, "ydata": 0.0, "zdata": 0.0}, &
  !     &"prs side1":{ "type": "pressure", "sides": ["ylo"], "data": 0.0},&
  !     &"prs side2":{ "type": "pressure", "sides": ["yhi"], "data": 1.0}}}'
  ! call lid_driven_cavity_test(paramstr, 2)

  ! if (this_rank == 0) print *, 'z-dir flow'
  ! paramstr = '{"viscous-implicitness": 0.5, "viscosity": 1.0, "density": 1.0, "max dt": 5e-2, &
  !     &"bc":{ &
  !     &"vel sides":{ "type": "velocity", "sides": ["xlo", "ylo", "xhi", "yhi"], &
  !     &              "xdata": 0.0, "ydata": 0.0, "zdata": 0.0}, &
  !     &"prs side1":{ "type": "pressure", "sides": ["zlo"], "data": 0.0},&
  !     &"prs side2":{ "type": "pressure", "sides": ["zhi"], "data": 1.0}}}'
  ! call lid_driven_cavity_test(paramstr, 3)

  call amrex_finalize()
  call exit(status)

contains

  subroutine lid_driven_cavity_test(paramstr, dir)

    use flow_sim_type

    character(*), intent(in) :: paramstr
    integer, intent(in) :: dir

    type(flow_sim) :: flow
    type(parameter_list), pointer :: params => null()
    character(:), allocatable :: errmsg
    real(r8) :: tmax
    integer :: i, ndumps

    call parameter_list_from_json_string(paramstr, params, errmsg)
    INSIST(associated(params))

    call flow%init(env, mesh, params)
    ! call flow%solve_to_time(5e-1_r8)
    ! call flow%write_solution()

    ndumps = 20
    tmax = 40
    flow%tout = [(i * tmax / ndumps, i=1,ndumps)]

    call flow%run()

    ! max_vel = max(flow%face_velocity(1)%norm0(1), flow%face_velocity(2)%norm0(1), &
    !     flow%face_velocity(3)%norm0(1))

    ! err = 7.367135328151381e-2_r8
    ! err = abs((max_vel) - err) / err
    ! if (this_rank == 0) print *, 'error: ', err
    ! if (err > 1e-3_r8) status = 1

  end subroutine lid_driven_cavity_test

end program test_lid_driven_cavity
