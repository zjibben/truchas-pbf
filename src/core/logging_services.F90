!!
!! LOGGING_SERVICES
!!
!! This module provides a common set of procedures for writing log/trace
!! messages -- the types of messages typically written to the terminal or
!! simulation log file.  All application code should use the facilities
!! provided here, and eschew use of raw Fortran writes for this purpose.
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! February 2014
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! PROGRAMMING INTERFACE
!!
!!  There are three categories of log messages. Informational messages are the
!!  typical generic messages, written as-is without any adornment.  These are
!!  logged using LS_INFO.  Warning messages are intended to warn the user of
!!  a possible non-fatal error or exceptional condition.  These are written
!!  prefixed with "Warning:" to be readily distinguished from other messages.
!!  These are logged using LS_WARN. The final category is fatal error messages,
!!  and these are logged using either LS_FATAL or LS_ERROR. The former prefixes
!!  the message with "FATAL:" and gracefully terminates execution. The latter
!!  prefixes the message with "ERROR:" but does not terminate execution.
!!  However in this case it is expected that execution is ultimately terminated
!!  using LS_FATAL; the intent is to permit the logging of multiple fatal
!!  errors before finally terminating execution.
!!
!!  Informational messages are subdivided into several verbosity levels.
!!  Only those messages not exceeding the system verbosity level are logged.
!!  The verbosity levels are provided by the named module constants
!!  LS_VERB_SILENT, LS_VERB_NORMAL, and LS_VERB_NOISY.  The read-only module
!!  variable LS_VERBOSITY gives the system verbosity level.  These values
!!  are of a private derived-type, and thus are only useful for passing to the
!!  procedures described below (the VERBOSITY argument), or in a comparison
!!  expression like "if (LS_VERBOSITY >= LS_VERB_NOISY) ..."  All of the
!!  comparison operators are defined for the type.
!!
!!  The procedures LS_INFO, LS_WARN, LS_ERROR, and LS_FATAL should be regarded
!!  as collective procedures, needing to be called by every parallel process.
!!  In reality only LS_FATAL is truly collective (to terminate execution.)
!!  Logging is done by the designated output process only, using the message on
!!  that process; other processes do nothing, except when terminating execution
!!  with LS_FATAL.
!!
!!  The procedure LS_PANIC is provided for situations where a graceful
!!  collective termination using LS_FATAL is not possible or convenient.
!!
!!  There are situations where it is not possible or convenient to gracefully
!!  terminate execution in a collective manner using LS_FATAL.  The procedure
!!  LS_PANIC exists for this purpose. It can be called by one or more processes,
!!  and each writes its message to standard output (only) prefixed with
!!  "PANIC[N]:", where N is the process rank, and then execution is terminated
!!  via MPI_ABORT, which is supposed to terminate all processes, including
!!  those not calling LS_PANIC. This is somewhat nasty and can result in some
!!  lost output, and perhaps even hung processes. This procedure should not be
!!  used lightly or or routinely.
!!
!!  What follows is a more detailed description of the procedures.
!!
!!  CALL LS_INITIALIZE(UNITS [,VERBOSITY] [,FINALIZE]) defines the destination
!!    for all log messages and sets the verbosity level.  This must be called
!!    before any of the other subroutines. UNITS is a rank-1 integer array
!!    containing the logical units where log messages are to be written; they
!!    must be connected for write access. If not specified, the verbosity level
!!    defaults to LS_VERB_NORMAL. The optional argument FINALIZE specifies a
!!    subroutine that should be called to finalize the client application
!!    before LS_EXIT and LS_FATAL terminate execution. The subroutine should
!!    have no arguments. The default action is to call MPI_FINALIZE when MPI
!!    is not already finalized.
!!
!!  CALL LS_INFO(MESSAGE [,VERBOSITY]) writes the message passed in the
!!    scalar character argument MESSAGE.  VERBOSITY specifies the verbosity
!!    level of the message, and defaults to LS_VERB_NORMAL.  If the message
!!    verbosity level exceeds the system verbosity level (LS_VERBOSITY) the
!!    message is not written.
!!
!!  CALL LS_INFO(MESSAGE, ADVANCE [,VERBOSITY]) writes the message passed
!!    in the scalar character argument MESSAGE.  If the logical argument
!!    ADVANCE is false, the usual trailing newline is not written, otherwise
!!    the behavior is exactly as for the first variant of LS_INFO.
!!
!!  CALL LS_WARN(MESSAGE) writes the warning message passed in the scalar
!!    character argument MESSAGE.  The written message is prefixed with the
!!    string "Warning:".
!!
!!  CALL LS_ERROR(MESSAGE) writes the error message passed in the scalar
!!    character argument MESSAGE.  The written message is prefixed with the
!!    string "ERROR:".  This procedure does not initiate termination of the
!!    simulation, however the caller is expected to ultimately do so using
!!    LS_FATAL.
!!
!!  CALL LS_FATAL(MESSAGE) writes the error message passed in the scalar
!!    character argument MESSAGE.  The written message is prefixed with the
!!    string "FATAL:".  After logging the message, execution is gracefully
!!    terminated with a nonzero exit code.
!!
!!  CALL LS_EXIT() logs the fixed normal termination message (no passed
!!    message) and execution is gracefully terminated with a 0 exit code.
!!
!!  CALL LS_PANIC(MESSAGE) writes the message passed in the scalar character
!!    argument MESSAGE to the terminal, prefixed with the string "PANIC[N]: ",
!!    where N is the rank of the process.  This is not a collective procedure;
!!    any number of processes may call it and each process writes its own
!!    message. After writing the message execution is ungracefully terminated
!!    via MPI_ABORT.
!!

module logging_services

  use simlog_type, only: simlog
  use simlog_type, only: LS_VERB_SILENT => VERB_SILENT, LS_VERB_NORMAL => VERB_NORMAL, &
                         LS_VERB_NOISY => VERB_NOISY
  implicit none
  private

  public :: LS_initialize
  public :: LS_info, LS_warn, LS_error, LS_fatal, LS_exit, LS_panic
  public :: LS_close
  
  public :: LS_VERB_SILENT, LS_VERB_NORMAL, LS_VERB_NOISY

  procedure(finalize_if), pointer :: finalize_client => null()
  abstract interface
    subroutine finalize_if
    end subroutine
  end interface

  type(simlog), allocatable :: logger

contains

  subroutine LS_initialize(units, verbosity, finalize)
    use mpi
    integer, intent(in) :: units(:)
    integer, intent(in), optional :: verbosity
    procedure(finalize_if), optional :: finalize
    allocate(logger)
    call logger%init(MPI_COMM_WORLD, units, verbosity, finalize)
  end subroutine LS_initialize

  subroutine LS_info(message, verbosity)
    character(*), intent(in) :: message
    integer, intent(in), optional :: verbosity
    call logger%info(message, verbosity)
  end subroutine LS_info

  subroutine LS_warn(message)
    character(*), intent(in) :: message
    call logger%warn(message)
  end subroutine LS_warn

  subroutine LS_error(message)
    character(*), intent(in) :: message
    call logger%error(message)
  end subroutine LS_error

  subroutine LS_exit
    call logger%exit
  end subroutine LS_exit

  subroutine LS_fatal(message)
    character(*), intent(in) :: message
    call logger%fatal(message)
  end subroutine LS_fatal

  subroutine LS_panic(message)
    character(*), intent(in) :: message
    call logger%panic(message)
  end subroutine LS_panic

  !! Temporary hack -- like LS_exit except don't exit
  subroutine LS_close()
    character(:), allocatable :: date, time, zone
    call timestamp(date, time, zone)
    call LS_info('Normal terminatation on ' // date // ' at '// time // ' ' // zone)
    call finalize_client
  end subroutine LS_close

  subroutine timestamp(date, time, zone)
    character(:), allocatable, intent(out) :: date, time, zone
    character(8)  :: d
    character(10) :: t
    character(5)  :: z
    call date_and_time(date=d, time=t, zone=z)
    date = d(1:4) // '-' // d(5:6) // '-' // d(7:8)
    time = t(1:2) // ':' // t(3:4) // ':' // t(5:6)
    zone = z(1:5)
  end subroutine timestamp

end module logging_services
