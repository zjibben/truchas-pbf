!!
!! HYPRE_STRUCT_HYBRID_TYPE
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! January 2018
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! IMPLEMENTATION NOTES
!!
!! (1) The HYPRE_StructHybridSetup function requires the RHS and solution
!! vectors as input arguments.  The reference manual indicates that "layout"
!! of their data, but not the data itself, may be used. Presumably this means
!! that that the same vectors must be passed to StructHybridSolve.
!!
!! (2) The SETUP procedure caches the Hypre object handles for the RHS and
!! solution vectors, and thus no arguments are needed for the SOLVE procedure.
!! Howver that makes APPLY opaque.  So I've opted to pass the RHS and solution
!! vectors explicitly to APPLY, and check internally via asserts that those
!! Hypre objects are the expected ones. I think this makes things clearer from
!! the perspective of the client.
!!
!! (3) The design intent of the StructVector interface is for Initialize and
!! Assemble to serve as re-initialize and re-assemble in a manner consistent
!! with everything else in Hypre. But it doesn't actually work that way. To
!! reuse Struct vectors, DO NOT call Initialize again (just the once), but
!! just reset the values using SetBoxValues. Assemble does nothing so do not
!! bother with it. Given this behavior, the Initialize call is moved to the
!! creation of the vector. See https://github.com/hypre-space/hypre/issues/92
!!

#include "f90_assert.fpp"

module hypre_struct_hybrid_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use fhypre
  use amrex_mesh_type
  implicit none
  private

  type, public :: hypre_struct_hybrid
    private
    integer :: comm ! MPI communicator
    type(amrex_mesh), pointer :: mesh => null() ! reference only -- not owned
    type(hypre_obj) :: grid = hypre_null_obj
    type(hypre_obj) :: stencil = hypre_null_obj
    type(hypre_obj) :: solver = hypre_null_obj
    type(hypre_obj) :: A = hypre_null_obj ! reference only -- not owned
    type(hypre_obj) :: b = hypre_null_obj ! reference only -- not owned
    type(hypre_obj) :: x = hypre_null_obj ! reference only -- not owned
    !! Hypre Struct solver parameters
    real(r8) :: tol
    integer,  allocatable :: max_ds_iter, max_pc_iter
    real(r8), allocatable :: conv_rate_tol
  contains
    procedure :: init
    procedure :: setup
    procedure :: solve
    procedure :: num_itr
    procedure :: num_dscg_itr
    procedure :: num_pcg_itr
    procedure :: rel_res_norm
    procedure :: create_vector
    procedure :: create_matrix
    procedure :: copy_multifab_to_vector
    procedure :: copy_vector_to_multifab
    final :: hypre_struct_hybrid_delete
  end type hypre_struct_hybrid

  integer, parameter :: NDIM = 3  ! ripe for a type parameter

contains

  !! Final subroutine for HYPRE_STRUCT_HYBRID objects
  subroutine hypre_struct_hybrid_delete(this)
    type(hypre_struct_hybrid), intent(inout) :: this
    integer :: ierr
    if (hypre_associated(this%solver)) call fHYPRE_StructHybridDestroy(this%solver, ierr)
    if (hypre_associated(this%grid)) call fHYPRE_StructGridDestroy(this%grid, ierr)
    if (hypre_associated(this%stencil)) call fHYPRE_StructStencilDestroy(this%stencil, ierr)
  end subroutine hypre_struct_hybrid_delete

  subroutine init(this, mesh, params, stat, errmsg, enabled_box)

    use parameter_list_type

    class(hypre_struct_hybrid), intent(out) :: this
    type(amrex_mesh), intent(in), target :: mesh
    type(parameter_list), intent(inout) :: params
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg
    logical, optional :: enabled_box(:)

    integer :: j, ierr
    character(:), allocatable :: context

    this%comm = mesh%comm
    this%mesh => mesh

    !! Create the Hypre Struct grid object.
    call fHYPRE_StructGridCreate(this%comm, NDIM, this%grid, ierr)
    do j = 1, mesh%nbox
      if (present(enabled_box)) then
        if (.not.enabled_box(j)) cycle
      end if
      call fHYPRE_StructGridSetExtents(this%grid, mesh%box(j)%lo, mesh%box(j)%hi, ierr)
    end do
    call fHYPRE_StructGridAssemble(this%grid, ierr)
    INSIST(ierr == 0)

    !! Create the Hypre Struct grid stencil object for the 7-point Laplacian.
    call fHYPRE_StructStencilCreate(NDIM, 7, this%stencil, ierr)
    call fHYPRE_StructStencilSetElement(this%stencil, 0, [0,0,0], ierr)
    call fHYPRE_StructStencilSetElement(this%stencil, 1, [-1,0,0], ierr)
    call fHYPRE_StructStencilSetElement(this%stencil, 2, [+1,0,0], ierr)
    call fHYPRE_StructStencilSetElement(this%stencil, 3, [0,-1,0], ierr)
    call fHYPRE_StructStencilSetElement(this%stencil, 4, [0,+1,0], ierr)
    call fHYPRE_StructStencilSetElement(this%stencil, 5, [0,0,-1], ierr)
    call fHYPRE_StructStencilSetElement(this%stencil, 6, [0,0,+1], ierr)
    INSIST(ierr == 0)

    !! Process the parameter list.
    context = 'processing ' // params%name() // ': '
    call params%get('tol', this%tol, default=1.0d-6, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context//errmsg
      return
    end if
    if (this%tol <= 0) then
      stat = 1
      errmsg = context//'"tol" must be > 0'
      return
    end if

    if (params%is_parameter('max-ds-iter')) then
      allocate(this%max_ds_iter)
      call params%get('max-ds-iter', this%max_ds_iter, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context//errmsg
        return
      end if
      if (this%max_ds_iter < 0) then
        stat = 1
        errmsg = context//'"max-ds-iter" must be >= 0'
        return
      end if
    end if

    if (params%is_parameter('max-pc-iter')) then
      allocate(this%max_pc_iter)
      call params%get('max-pc-iter', this%max_pc_iter, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context//errmsg
        return
      end if
      if (this%max_pc_iter < 0) then
        stat = 1
        errmsg = context//'"max-pc-iter" must be >= 0'
        return
      end if
    end if

    if (params%is_parameter('conv-rate-tol')) then
      allocate(this%conv_rate_tol)
      call params%get('conv-rate-tol', this%conv_rate_tol, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context//errmsg
        return
      end if
      if (this%conv_rate_tol < 0) then
        stat = 1
        errmsg = context//'"conv-rate-tol" must be >= 0'
        return
      end if
    end if

  end subroutine init

  subroutine setup(this, A, b, x)

    class(hypre_struct_hybrid), intent(inout) :: this
    type(hypre_obj), intent(in) :: A
    type(hypre_obj), intent(in) :: b, x ! see Note 1

    integer :: ierr

    this%A = A
    this%b = b
    this%x = x

    call fHYPRE_ClearAllErrors

    !! Create the Hypre solver object.  Once the solver has been setup, it is
    !! not possible to change the matrix values without completely destroying
    !! the solver and recreating it from scratch.
    if (hypre_associated(this%solver)) call fHYPRE_StructHybridDestroy(this%solver, ierr)
    call fHYPRE_StructHybridCreate(this%comm, this%solver, ierr)
    INSIST(ierr == 0)

    !! Set solver parameters.
    call fHYPRE_StructHybridSetTol(this%solver, this%tol, ierr)

    !! Set optional solver parameters (get Hypre defaults otherwise)
    if (allocated(this%max_ds_iter)) &
        call fHYPRE_StructHybridSetDSCGMaxIter(this%solver, this%max_ds_iter, ierr)
    if (allocated(this%max_pc_iter)) &
        call fHYPRE_StructHybridSetPCGMaxIter(this%solver, this%max_pc_iter, ierr)
    if (allocated(this%conv_rate_tol)) &
        call fHYPRE_StructHybridSetConvergenceTol(this%solver, this%conv_rate_tol, ierr)
    INSIST(ierr == 0)

    !! Setup the solver.
    call fHYPRE_StructHybridSetup(this%solver, A, b, x, ierr)
    INSIST(ierr == 0)

  end subroutine setup


  subroutine solve(this, b, x, stat)

    class(hypre_struct_hybrid), intent(in) :: this
    type(hypre_obj), intent(in) :: b, x
    integer, intent(out) :: stat

    integer :: ierr

    INSIST(hypre_associated(b, this%b)) ! See Note 2
    INSIST(hypre_associated(x, this%x)) ! See Note 2

    call fHYPRE_ClearAllErrors

    !! Solve the system.  I would expect IERR to return HYPRE_ERROR_CONV if
    !! the error tolerance is not achieved but this seems not to be the case,
    !! so we test the residual norm directly.
    call fHYPRE_StructHybridSolve(this%solver, this%A, b, x, ierr)
    INSIST(ierr == 0 .or. ierr == HYPRE_ERROR_CONV)
    if (ierr == HYPRE_ERROR_CONV .or. this%rel_res_norm() > this%tol) then
      stat = 1
      call fHYPRE_ClearAllErrors
    else
      stat = 0
    end if

  end subroutine solve

  !! Return the number of iterations (total)
  integer function num_itr(this)
    class(hypre_struct_hybrid), intent(in) :: this
    integer :: ierr
    call fHYPRE_StructHybridGetNumIterations(this%solver, num_itr, ierr)
  end function num_itr

  !! Return the number of iterations with diagonal scaling preconditioning
  integer function num_dscg_itr(this)
    class(hypre_struct_hybrid), intent(in) :: this
    integer :: ierr
    call fHYPRE_StructHybridGetDSCGNumIterations(this%solver, num_dscg_itr, ierr)
  end function num_dscg_itr

  !! Return the number of iterations using multigrid preconditioning
  integer function num_pcg_itr(this)
    class(hypre_struct_hybrid), intent(in) :: this
    integer :: ierr
    call fHYPRE_StructHybridGetPCGNumIterations(this%solver, num_pcg_itr, ierr)
  end function num_pcg_itr

  !! Return the relative residual norm
  real(r8) function rel_res_norm(this)
    class(hypre_struct_hybrid), intent(in) :: this
    integer :: ierr
    call fHYPRE_StructHybridGetFinalRelativeResidualNorm(this%solver, rel_res_norm, ierr)
  end function rel_res_norm

  !! Create a Hypre StructVector over the grid.
  subroutine create_vector(this, vector)
    class(hypre_struct_hybrid), intent(in) :: this
    type(hypre_obj), intent(out) :: vector
    integer :: ierr
    call fHYPRE_ClearAllErrors
    call fHYPRE_StructVectorCreate(this%comm, this%grid, vector, ierr)
    call fHYPRE_StructVectorInitialize(vector, ierr)  ! See Note 3
    INSIST(ierr == 0)
  end subroutine create_vector

  !! Create a Hypre StructMatrix over the grid.
  subroutine create_matrix(this, matrix)
    class(hypre_struct_hybrid), intent(in) :: this
    type(hypre_obj), intent(out) :: matrix
    integer :: ierr
    call fHYPRE_ClearAllErrors
    call fHYPRE_StructMatrixCreate(this%comm, this%grid, this%stencil, matrix, ierr)
    INSIST(ierr == 0)
  end subroutine create_matrix

  !! Copy component data from a cell-centered multifab to a Hypre Struct vector
  !! created by CREATE_VECTOR.  The multifab must have no ghosts (IMPORTANT!)

  subroutine copy_multifab_to_vector(this, mf, comp, vector)

    use amrex_base_module, only: amrex_multifab, amrex_mfiter, amrex_box

    class(hypre_struct_hybrid), intent(in) :: this
    type(amrex_multifab), intent(in) :: mf
    integer, intent(in) :: comp
    type(hypre_obj), intent(in) :: vector

    integer :: ierr
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx
    real(r8), pointer, contiguous :: dp(:,:,:,:)

    call fHYPRE_ClearAllErrors
    !call fHYPRE_StructVectorInitialize(vector, ierr) ! See Note 3
    call this%mesh%mfiter_build(mfi)
    do while (mfi%next())
      bx = mfi%tilebox()
      dp => mf%dataptr(mfi)
      call fHYPRE_StructVectorSetBoxValues(vector, bx%lo, bx%hi, dp(:,:,:,comp), ierr)
    end do
    !call fHYPRE_StructVectorAssemble(vector, ierr) ! See Note 3
    INSIST(ierr == 0)

  end subroutine copy_multifab_to_vector

  !! Copy data from a Hypre Struct vector created by CREATE_VECTOR to a cell-
  !! centered multifab component. The multifab must have no ghosts (IMPORTANT!)
  !! This will set all data in the multifab.

  subroutine copy_vector_to_multifab(this, vector, mf, comp)

    use amrex_base_module, only: amrex_multifab, amrex_mfiter, amrex_box

    class(hypre_struct_hybrid), intent(in) :: this
    type(hypre_obj), intent(in) :: vector
    type(amrex_multifab), intent(in) :: mf
    integer, intent(in) :: comp

    integer :: ierr
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx
    real(r8), pointer, contiguous :: dp(:,:,:,:)

    call fHYPRE_ClearAllErrors

    call this%mesh%mfiter_build(mfi)
    do while (mfi%next())
      bx = mfi%tilebox()
      dp => mf%dataptr(mfi)
      call fHYPRE_StructVectorGetBoxValues(vector, bx%lo, bx%hi, dp(:,:,:,comp), ierr)
      INSIST(ierr == 0)
    end do

  end subroutine copy_vector_to_multifab

end module hypre_struct_hybrid_type
