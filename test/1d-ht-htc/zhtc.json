// HTC boundary condition test -- z direction
//
// This solves $u_t = k u_{xx}$ in $[a,1-a]$ with the HTC boundary conditions
// $k u_x = \alpha (u-1)$ at $x=a$ and $-k u_x = \alpha (u-1)$ at $x=1-a$.
// This has the solution $u(x,t) = 1 + e^{-k\pi^2 t} \sin\pi x$, when the HTC
// $\alpha = k\pi/\tan\pi a$. For this simulation we take $a=1/8$, $k=1/\pi^2$.
//
// We immerse this 1D problem in a 3D problem in the obvious way, solving on
// a cube domain, picking one of the coordinate directions for "x" and applying
// no flux conditions on the other boundaries.
//
// The initial condition is $u(x) = 1 + \sin\pi x$, and the solution is output
// at $t=1/2, 1, 2$.  The maximum of $u$ is $1 + e^{-t}$.

{
  "mesh": {
    "num-levels": 1,
    "lo": [0,0,0],
    "hi": [7,3,63],
    "box-size": 16,
    "prob-lo": [0.125, 0.125, 0.125],
    "prob-hi": [0.875, 0.875, 0.875]
  },
  "model": {
    "material": {
      "density": 1.0,
      "solid": {
        "conductivity": 0.1013211836423378, // $1/\pi^2$
        "specific-heat": 1.0
      },
      "liquid": {
        "conductivity": 0.1013211836423378, // $1/\pi^2$
        "specific-heat": 1.0
      },
      "sigma-func-type": "smoother-step",
      "solidus-temp": 100.0,    // ensure phase change not in play
      "liquidus-temp": 110.0,
      "latent-heat": 0.0,
      "smoothing-radius": 1.0
    },
    "heat": {
      "discretization":"finite-volume",
      "bc": {
        "xsides": {
          "type": "htc",
          "sides": ["zlo", "zhi"],
          "coefficient": 0.7684680442623437, // $1/(\pi\tan(\pi/8))$
          "ambient-temp": 1.0
        },
        "adiabatic": {
          "type": "flux",
          "sides": ["xlo", "xhi", "ylo", "yhi"],
          "data": 0.0
        }
      }
    },
    "disable-flow": true
  },
  "solver": {
    "heat": {
      "temp-rel-tol": 1.0e-4,
      "num-cycles": 1,
      "nlk-max-itr": 5,
      "nlk-tol": 0.01
    }
  },
  "sim-control": {
    "initial-time": 0.0,
    "initial-time-step": 1.0e-3,
    "min-time-step": 1.0e-5,
    "output-times": [0.5, 1.0, 2.0]
  },
  "initial-temperature": {"type":"TEST1", "dim":3},

  // This sublist is consumed by AMREX_INIT
  "amrex": {
    "amrex": {"fpe_trap_invalid": 1}
  }
}
