# NIST Single-Track Demo Problem

This demo problem simulates a single 1mm laser weld track on Inconel 625 plate.
It is a variation of the 2018 NIST AM Bench single track challenge problem.

The "ht" input files solve heat transfer/phase change only, while the remaining
input files include surface tension driven flow in the melt pool.

