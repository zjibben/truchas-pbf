// NIST Single Track Demo Problem: 5 micron
//
// NIST single track weld on Inconel 625 plate demo problem:
// * 5 micron grid resolution
// * heat transfer/phase change with surface tension driven flow in the melt pool.
//
// Model is posed in mm, g, ms, kelvin units.
//
// Conversion factors from MKS units:
//   1 Joule [kg m^2 / s^2] = 1e3 g mm^2 / ms^2
//   1 Watt [J/s] = 1 [g mm^2 / ms^3]
//   1 W/m^2 = 1e-6 [g mm^2 / s^3 mm^2]
//   1 N [kg m / s^2] = 1 g mm / ms^2
//   1 Pa [N/m^2] = 1e-6 (g-mm/ms^2)/mm^2
//   1 Pa-s = 1 kg / m s = 1e-3 g/mm-ms

{
   "physical-constants": {
      "stefan-boltzmann": 5.67e-14
   },
   "mesh": {
      "box-size": 20,
      "prob-lo": [0.0, 0.0, -1.6],
      "prob-hi": [3.2, 1.6, 0.0],

      "num-levels": 7, // 5 um
      "lo": [0,0,0,  4,0,6,  10,0,16,  22,0,36,  48,0,74, 100,0,152,  204,0,306],
      "hi": [9,4,4, 15,3,9,  29,5,19,  57,5,39, 111,7,79, 219,11,159, 435,19,319]
   },
   "laser-scan-path": {
      "laser": {
         "type": "gaussian",
         "power": 195.0, // [W]
         "sigma": 0.035  // [mm]
      },
      "laser-absorp": 0.4,
      "laser-time-constant": 0.01,
      "scan-path": {
         "start-coord": [1.1, 0.0],
         "command-file": "path.json"
      }
   },
   "model": {
       "material": {
          "density": 7.57e-3,
          "solid": {
             "specific-heat": {"type":"polynomial", "poly-center":273.0,
                               "poly-coef":[428.38, 0.23638], "poly-powers":[0, 1]},
             "conductivity": {"type":"polynomial", "poly-center":270.0,
                              "poly-coef":[12.3e-3, 1.472e-5], "poly-powers":[0, 1]}
          },
          "liquid": {
             "specific-heat": 750.65,
             "conductivity": {"type":"polynomial", "poly-center":270.0,
                              "poly-coef":[8.92e-3, 1.474e-5], "poly-powers":[0, 1]},
             "viscosity": {"type": "polynomial", "poly-center": 273.0,
                           "poly-coef": [4.0493e-6, -1.9397e-2, 34.725],
                           "poly-powers": [0, -1, -2]},
             "density-deviation": {"type": "polynomial", "poly-center": 273.0,
                                   "poly-coef": [0.13657, -9.0197e-5, -7.9917e-9],
                                   "poly-powers": [0, 1, 2]}
          },
          "sigma-func-type": "smooth-step",
          //"solidus-temp": 1410.0, // eutectic
          "solidus-temp": 1531.0,
          "liquidus-temp": 1616.0,
          "latent-heat": 2.1754e5  // [g-mm^2/ms^2/g]
       },
       "heat": {
          "bc": {
             "top": {
                "type": "flux",
                "sides": ["zhi"],
                "data": "laser"
             },
             "top-rad": {
                "type": "radiation",
                "sides": ["zhi"],
                "emissivity": 0.6,
                "ambient-temp": 300.0
             },
             "bottom": {
                "type": "htc",
                "sides": ["zlo"],
                "coefficient": 1.0e-4, // [W/mm^2-K]
                "ambient-temp": 300.0
             },
             "adiabatic": {
                "type": "flux",
                "sides": ["xlo", "xhi", "ylo", "yhi"],
                "data": 0.0
             }
          }
       },
       "flow":{"inviscid":false, "body-force-density":9.81e-3}
       //,"disable-flow": true
    },
    "solver": {
        "microstructure": {
           "exaca": {"liquidus-temp": 1616.0}
        },
        "heat": {
           "temp-rel-tol": 1.0e-2,
           "num-cycles": 4,
           "nlk-max-itr": 5,
           "nlk-tol": 0.01
        },
       "flow":{
          "viscous-implicitness": 1.0,
          "courant-number": 0.5,
          "bc": {
             "symmetry": {"type": "symmetry", "sides":["ylo"],
                          "xdata":0.0, "ydata":0.0, "zdata":0.0},
             "solid-sides": {"type": "velocity", "sides":["xlo","xhi","yhi","zlo"],
                             "xdata":0.0, "ydata":0.0, "zdata":0.0},
             "surface": {"type":"marangoni", "sides":["zhi"],
                         "dsigma": -0.11e-7} // [(g-mm/ms^2)/(mm-K)]
          },
          "pressure-solver":{
             //"tol": 1e-6,         // hypre default
             //"max-ds-iter": 1000, // hypre default
             //"max-pc-iter": 200,  // hypre default
             //"conv-rate-tol": 0.9 // hypre default
          },
          "viscous-solver":{
             //"tol": 1e-6,         // hypre default
             //"max-ds-iter": 1000, // hypre default
             //"max-pc-iter": 200,  // hypre default
             //"conv-rate-tol": 0.9 // hypre default
          }
       }
    },
    "sim-control": {
       "initial-time": 0.0,
       "initial-time-step": 1.0e-6,
       "min-time-step": 1.0e-9,
       "output-times": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.25, 2.0]
    },
    "initial-temperature": {
       "type": "constant",
       "value": 300.0
    },

    // This sublist is consumed by AMREX_INIT
    "amrex": {
       "amrex": {"fpe_trap_invalid": 1}
    }
}
