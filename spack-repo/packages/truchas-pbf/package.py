# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class TruchasPbf(CMakePackage):
    """A spin-off of Truchas specialized for modeling metal powder bed fusion processes."""

    homepage = "https://github.com/truchas-pbf/truchas-pbf"
    git      = "ssh://git@github.com/truchas-pbf/truchas-pbf.git"

    maintainers("zjibben")

    version("develop", branch="master")

    ### Variants #################################
    # variant("unit", default=False, description="Enable Unit Tests")
    # variant("documentation", default=False, description="Build Sphinx Documentation")
    variant("postprocessing", default=False, description="Include postprocessing tools")
    variant("config", default=True, description="Use proved Truchas-PBF config files for cmake")

    ### Dependencies #############################
    depends_on("mpi")
    depends_on("cmake@3.21:", type="build")
    depends_on("amrex@24.04 +shared +fortran +hypre +plotfile_tools")
    depends_on("hypre@2.31: +shared ~fortran")
    depends_on("petaca@23.11")
    depends_on("py-yt ~astropy", when="+postprocessing")


    def cmake_args(self):
        # opts = [
        #     self.define_from_variant("BUILD_TESTING", "unit"),
        #     self.define_from_variant("BUILD_DOC", "documentation")
        # ]
        opts = []

        spec = self.spec
        if "+config" in spec:
            root = self.root_cmakelists_dir

            nag = "nag" in self.compiler.fc

            if spec.satisfies("platform=linux"):
                if nag or "%nag" in spec:
                    opts.append("-C {}/config/linux-nag.cmake".format(root))
                elif "%gcc" in spec:
                    opts.append("-C {}/config/linux-gcc.cmake".format(root))
                elif "%intel" in spec:
                    opts.append("-C {}/config/linux-intel.cmake".format(root))

            elif spec.satisfies("platform=darwin"):
                if "%apple-clang" in spec:
                    opts.append("-C {}/config/mac-gcc-clang.cmake".format(root))

        return opts
